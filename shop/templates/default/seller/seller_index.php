<?php defined('In33hao') or exit('Access Invalid!');?>

<div class="ncsc-index">
  <div class="top-container">
    <div class="basic-info">
      <dl class="ncsc-seller-info">
        <dt class="seller-name">
          <h3><?php echo $_SESSION['seller_name']; ?></h3>  
          <h5>(用户名：<?php echo $_SESSION['member_name']; ?>)
		  <!--
		  <?php if($output['store_info']['is_distribution']==0){
		  	if($output['distribution_info']){
		 ?>
		 	<b style=" background:#4FC0E8;padding:5px; color:#fff;">已提交申请，请等待审核</b>
		 <?php }else{
				echo '<a href="index.php?act=seller_center&op=distribution" style=" background:#48CFAE;padding:5px; color:#fff;">申请开通分销</a>';
				}
		 }else{
		 	echo '<b style=" background:#b1191a;padding:5px; color:#fff;">分销店铺</b>';
		 }?>
         -->
         </h5>
        </dt>
        <dd class="store-logo">
          <p><img src="<?php echo getStoreLogo($output['store_info']['store_label'],'store_logo');?>"/></p>
          <a href="<?php echo urlShop('store_setting', 'store_setting');?>"><i class="icon-edit"></i>编辑店铺设置</a> </dd>
        <dd class="seller-permission">管理权限：<strong><?php echo $_SESSION['seller_group_name'];?></strong></dd>
        <dd class="seller-last-login">最后登录：<strong><?php echo $_SESSION['seller_last_login_time'];?></strong> </dd>
        <dd class="store-name"><?php echo $lang['store_name'].$lang['nc_colon'];?><a href="<?php echo urlShop('show_store', 'index', array('store_id' => $_SESSION['store_id']), $output['store_info']['store_domain']);?>" ><?php echo $output['store_info']['store_name'];?></a></dd>
        <dd class="store-grade"><?php echo $lang['store_store_grade'].$lang['nc_colon'];?><strong><?php echo $output['store_info']['grade_name']; ?></strong></dd>
        <dd class="store-validity"><?php echo $lang['store_validity'].$lang['nc_colon'];?><strong><?php echo $output['store_info']['store_end_time_text'];?>
          <?php if ($output['store_info']['reopen_tip']) {?>
          <a href="index.php?act=store_info&op=reopen">马上续签</a>
          <?php } ?>
          </strong> </dd>
      </dl>

    </div>
  </div>
  <div class="seller-cont">
      
    <div class="container type-a">
       
       <table class="ncsc-default-table order">
          <thead>
            <tr>
              <th class="w50">编号</th>
              <th class="w100">收入/支出</th>
              <th class="w100">来源</th>
              <th class="w100">产生时间</th>
            </tr>
          </thead>
          <tbody>
              <?php foreach($output['money_list'] as $money ){ ?>
              <tr >
                  <th class="w50" ><?php echo $money['id']; ?></th>
                  <th class="w100 center"><?php echo $money['money']; ?></th>
                  <th class="w100 center"><?php echo $money['from']; ?></th>
                  <th class="w100 center"><?php echo $money['time']; ?></th>
              </tr> 
              <?php } ?>
          </tbody>
          <tfoot>
            <?php if (is_array($output['money_list']) and !empty($output['money_list'])) { ?>
            <tr>
              <td colspan="20"><div class="pagination"><?php echo $output['show_page']; ?></div></td>
            </tr>
            <?php } ?>
          </tfoot>
        </table>
    </div>
    <div class="container type-c">
      <div class="hd">
        <h3>用户余额</h3>
        <!--<h5>按周期统计商家店铺的订单量和订单金额</h5>-->
      </div>
      <div class="content">
        <table class="ncsc-default-table">
          <thead>
            <tr>
              <th class="w50">类型</th>
              <th>数值</th>
              <th class="w100">操作</th>
            </tr>
          </thead>
          <tbody>
            <tr class="bd-line">
              <td>香券</td>
              <td><?php echo $output['member']['member_points']; ?></td>
              <td></td>
            </tr>
            <tr class="bd-line">
              <td>余额</td>
              <td id="yue"><?php echo $output['member']['available_rc_balance']; ?></td>
              <td><a class="go_cash ncbtn ncbtn-mint" nc_type="dialog" href="javascript:void(0)" dialog_title='提现'  uri="index.php?act=seller_center&op=cash_state&state_type=cash_cancel">提现</button></a>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
   
    
  </div>
</div>
<div class='fade'></div>
<script>
   
  
$(function(){
    $('.go_cash').click(function(){

    })
});
</script>
