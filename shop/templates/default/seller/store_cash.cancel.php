<style type="text/css">
    .tx_title{
      padding: 20px 40px;
      font-size: 18px;
    }
    .tx_input{
         padding: 10px 40px;
         font-size: 35px;
         color: #333333;
         border-bottom: 1px solid #EAEAEA;
         font-weight: bold
    }
    .tx_input span{
      margin-top: 8px;
      display: inline-block;
    }
    .tx_input input{
        height: 30px;
        font-size: 25px;
        font-weight: bold;
        border:none;

    }
    .choice{
      padding: 0px 40px;
    }
    .choice li{
      float: left;
      height: 100px;
      width: 90px;
      border:1px solid #eaeaea;
      margin-right: 20px;
    }
    .choice li i{
      height: 70%;
      width: 100%;
      display: block;
    }
    .choice li.active{
      border:1px solid #333333;
    }
    .choice li .zfb{
      background: url(http://shop.tianxiangmall.net/wap/images/zfb_icon.png);
      background-repeat: no-repeat;
      background-position: center;
    }
    .choice li .wx{
      background: url(http://shop.tianxiangmall.net/wap/images/wx_pay_icon.png);
      background-repeat: no-repeat;
      background-position: center;
    }
     .choice li div{
      text-align: center;
     }
     .btn_next{
        margin: 40px 0px;
     }
     .btn_next span{
        display: block;
        width: 80%;
        height: 50px;
        line-height: 50px;
        text-align: center;
        margin: 0 auto;
        background: #333333;
        color: #fff;
        border-radius: 25px;
     }
     .tianxie{
       padding:0px 40px;
     }
     .tianxie div{
      margin-top: 20px;
     }
     .tianxie input{
        font-size: 18px;
        color: #333333;
        height: 30px;
     }
     .tx_none{
         font-size: 20px;
         text-align: center;
         padding: 50px;
         color:#333333;
     }
</style>
<?php defined('In33hao') or exit('Access Invalid!');?>
<div class="eject_con">
  <div id="warning"></div>
  <form method="post" id="order_cancel_form"  action="index.php?act=seller_center&op=go_cash">
      <div class="tx_none">该功能暂未开放</div> 
<!--   
    <div class="tx_title">提现金额</div>
    <div class="tx_input"><span>¥</span> <input placeholder="请输入金额" type="text" name="cash"></div>
    <div class="tx_title">提现方式</div>
    <ul class='choice'>
        <li class="active">
            <i class="zfb"></i>
            <div>支付宝提现</div>
        </li>

    </ul>
    <div class="tianxie">
      <div><input type="text" placeholder="支付宝账号原名" name="name"></div>
      <div><input type="text" placeholder="支付宝用户名" name="uname"></div>
    </div>
    <div class="btn_next">
      <input id="confirm_button"  type="submit"  value="下一步">
    </div>-->
  </form>
</div>
<script type="text/javascript">
$(function(){
        $('#cancel_button').click(function(){
            DialogManager.close('seller_order_cancel_order');
         });
       $("input[name='state_info']").click(function(){
        if ($(this).attr('flag') == 'other_reason')
        {
            $('#other_reason').show();
        }
        else
        {
            $('#other_reason').hide();
        }
    });


});
</script> 
