<?php
/**
 * 商户中心

 */



defined('In33hao') or exit('Access Invalid!');

class seller_centerControl extends BaseSellerControl {

    /**
     * 构造方法
     *
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * 商户中心首页
     *
     */
    public function indexOp() {
        
       
        $id=$_SESSION['member_id'];
       
        
        $seller=Model('store_joinin')->where(['member_id'=>$id])->find();
        if($seller){
            $this->seller_indexOp();
            exit();
        }
        Language::read('member_home_index');
        // 店铺信息
        $store_info = $this->store_info;
        if(intval($store_info['store_end_time']) > 0) {
            $store_info['store_end_time_text']  = date('Y-m-d', $store_info['store_end_time']);
            $reopen_time = $store_info['store_end_time'] -3600*24 + 1  - TIMESTAMP;
            if (!checkPlatformStore() && $store_info['store_end_time'] - TIMESTAMP >= 0 && $reopen_time < 2592000) {
                //到期续签提醒(<30天)
                $store_info['reopen_tip'] = true;
            }
        } else {
            $store_info['store_end_time_text'] = L('store_no_limit');
        }
        // 店铺等级信息
        $store_info['grade_name'] = $this->store_grade['sg_name'];
        $store_info['grade_goodslimit'] = $this->store_grade['sg_goods_limit'];
        $store_info['grade_albumlimit'] = $this->store_grade['sg_album_limit'];

        Tpl::output('store_info',$store_info);
        // 商家帮助
//         $model_help = Model('help');
//         $condition  = array();
//         $condition['help_show'] = '1';//是否显示,0为否,1为是
//         $help_list = $model_help->getStoreHelpTypeList($condition, '', 6);
//         Tpl::output('help_list',$help_list);

        // 销售情况统计
        $field = ' COUNT(*) as ordernum,SUM(order_amount) as orderamount ';
        $where = array();
        $where['store_id'] = $_SESSION['store_id'];
        $where['order_isvalid'] = 1;//计入统计的有效订单
        // 昨日销量
        $where['order_add_time'] = array('between',array(strtotime(date('Y-m-d',(time()-3600*24))),strtotime(date('Y-m-d',time()))-1));
        $daily_sales = Model('stat')->getoneByStatorder($where, $field);
        Tpl::output('daily_sales', $daily_sales);
        // 月销量
        $where['order_add_time'] = array('gt',strtotime(date('Y-m',time())));
        $monthly_sales = Model('stat')->getoneByStatorder($where, $field);
        Tpl::output('monthly_sales', $monthly_sales);
        unset($field,$where);

        //单品销售排行
        //最近30天
        $stime = strtotime(date('Y-m-d',(time()-3600*24))) - (86400*29);//30天前
        $etime = strtotime(date('Y-m-d',time())) - 1;//昨天23:59
        $where = array();
        $where['store_id'] = $_SESSION['store_id'];
        $where['order_isvalid'] = 1;//计入统计的有效订单
        $where['order_add_time'] = array('between',array($stime,$etime));
        $field = ' goods_id,min(goods_name) as goods_name,SUM(goods_num) as goodsnum,min(goods_image) as goods_image ';
        $orderby = 'goodsnum desc,goods_id';
        $goods_list = Model('stat')->statByStatordergoods($where, $field, 0, 8, $orderby, 'goods_id');
        unset($stime,$etime,$where,$field,$orderby);
        Tpl::output('goods_list', $goods_list);

        if (!checkPlatformStore()) {
            if (C('groupbuy_allow') == 1){
                // 抢购套餐
                $groupquota_info = Model('groupbuy_quota')->getGroupbuyQuotaCurrent($_SESSION['store_id']);
                Tpl::output('groupquota_info', $groupquota_info);
            }
            if (intval(C('promotion_allow')) == 1){
                // 限时折扣套餐
                $xianshiquota_info = Model('p_xianshi_quota')->getXianshiQuotaCurrent($_SESSION['store_id']);
                Tpl::output('xianshiquota_info', $xianshiquota_info);
                // 满即送套餐
                $mansongquota_info = Model('p_mansong_quota')->getMansongQuotaCurrent($_SESSION['store_id']);
                Tpl::output('mansongquota_info', $mansongquota_info);
                // 优惠套装套餐
                $binglingquota_info = Model('p_bundling')->getBundlingQuotaInfoCurrent($_SESSION['store_id']);
                Tpl::output('binglingquota_info', $binglingquota_info);
                // 推荐展位套餐
                $boothquota_info = Model('p_booth')->getBoothQuotaInfoCurrent($_SESSION['store_id']);
                Tpl::output('boothquota_info', $boothquota_info);
            }
            if (C('voucher_allow') == 1){
                $voucherquota_info = Model('voucher')->getCurrentQuota($_SESSION['store_id']);
                Tpl::output('voucherquota_info', $voucherquota_info);
            }
        } else {
            Tpl::output('isOwnShop', true);
        }
        $phone_array = explode(',',C('site_phone'));
        Tpl::output('phone_array',$phone_array);

        //系统公告
        $model_message  = Model('article');
        $condition = array();
        $condition['ac_id'] = 1;
        $condition['article_position_in'] = ARTICLE_POSIT_ALL.','.ARTICLE_POSIT_SELLER;
        $condition['limit'] = 6;
        $article_list  = $model_message->getArticleList($condition);
        Tpl::output('article_list',$article_list);
		//三级分销权限信息
		$distribution_info = Model('store_distribution')->where(array('distri_store_id'=>$store_info['store_id']))->find();
		Tpl::output('distribution_info',$distribution_info);

        Tpl::output('menu_sign','index');
        Tpl::showpage('index');
    }
    
    
    //分销商首页
    public function seller_indexOp() {
          $model_order = Model('orders');
        Language::read('member_home_index');
        // 店铺信息
        $store_info = $this->store_info;
        if(intval($store_info['store_end_time']) > 0) {
            $store_info['store_end_time_text']  = date('Y-m-d', $store_info['store_end_time']);
            $reopen_time = $store_info['store_end_time'] -3600*24 + 1  - TIMESTAMP;
            if (!checkPlatformStore() && $store_info['store_end_time'] - TIMESTAMP >= 0 && $reopen_time < 2592000) {
                //到期续签提醒(<30天)
                $store_info['reopen_tip'] = true;
            }
        } else {
            $store_info['store_end_time_text'] = L('store_no_limit');
        }
        // 店铺等级信息
        $store_info['grade_name'] = $this->store_grade['sg_name'];
        $store_info['grade_goodslimit'] = $this->store_grade['sg_goods_limit'];
        $store_info['grade_albumlimit'] = $this->store_grade['sg_album_limit'];

        Tpl::output('store_info',$store_info);
        //获取用户信息
        $member=Model('member')->where(['member_id'=>$_SESSION['member_id']])->find();
        
        Tpl::output('member',$member);
        
        
        $money_list=Model('recharge_log')->where(['member_id'=>$_SESSION['member_id']])->order('id desc')->page(10)->master(false)->select();
        foreach($money_list as $k=>$v){
            $money_list[$k]['time']= date('Y-m-d H:i:s',$v['ctime']);
            if($v['pay_from'] ==1 ){
                $pay='支付宝充值';
            }elseif($v['pay_from']==2){
                $pay='微信充值';
            }elseif($v['pay_from']==3){
                $pay='余额消费';
            }elseif($v['pay_from']==4){
                $pay='分红';
            }
            $money_list[$k]['from']=$pay;
        }
        
        
        Tpl::output('money_list',$money_list);
        Tpl::output('show_page',$model_order->showpage());
       
		

        Tpl::output('menu_sign','index');
        Tpl::showpage('seller_index');
    }
    
    public function go_cashOp(){
          $member_id=$_SESSION['member_id'];
      
               $inc_file = BASE_PATH.DS.'api'.DS.'payment'.DS.'alipay_cash'.DS.'transfers.php';
          $info=Model('member')->where(['member_id'=>$member_id])->find();
          if(($info['available_rc_balance']-1000)<round($_POST['cash'],2)){
              if($info['available_rc_balance']<1000){
                  showmessage('只能提取超出1000的部分');
              }
              showmessage('余额不足');
          }
      require($inc_file);
       
      $appid = '2018041902581911';  //https://open.alipay.com 账户中心->密钥管理->开放平台密钥，填写添加了电脑网站支付的应用的APPID
    $outTradeNo = uniqid();     //商户转账唯一订单号
    $payAmount =round($_POST['cash'],2);          //转账金额，单位：元 （金额必须大于等于0.1元)
    $remark = 'TIANXIANG';    //转帐备注
    $signType = 'RSA2';       //签名算法类型，支持RSA2和RSA，推荐使用RSA2
//商户私钥，填写对应签名算法类型的私钥，如何生成密钥参考：https://docs.open.alipay.com/291/105971和https://docs.open.alipay.com/200/105310
//$saPrivateKey='MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAMg/8eQ+PvveCdXUqICy1JZCTiqxekk8cvs5hMr0D1A6vooISzIxSWSBie3n+8B4QTR9B6P1Udamhco4KrzjWwdByVrvsMzPIn//7YAulPKRT8U6PdzoaqlilDLTwpTqj0SK6VM4ag4ecOVD1cjZhG+MGyLT0PdU/XuTwOHBmtGnAgMBAAECgYEApoXefqJwt/x+C06rzVJpTIbY/9HU0kAtkdLUJf3tGhevyZN6DMRFtwmuDKQEi5q1BpQOnX7szmZuDarFh4w9bgLMUN0GXbP5fZPGK6vs/CFRYLExUyrPBlHzmIHjMLLSAC0tD2WDGJSnXfNCkRh0MTvK62/Ux5+SBWhwAIOGOGkCQQD0ixhqnEES0U/rnTxfdJr/mCTYEuHkkRbm25lrA7K5+oKq1SKeNHxVvuikUYoyqDUeH4J6B96PZnmwV8ddW2RTAkEA0aGek5uoErIMh1fQ69aKv/vQHXbwfgzVZtBLjtep96L/6YPDT7/fZP1p7CRKZlTmRuZ/JrENvahiBsjE688y3QJAUBJXfVkKibHVvG1wvkS9F+HmdoXAR4omeJMBKiQ82l6neG5vdmPzLlRjcqJsYOfo9KMWowR+oG/Keq5TTkNFlwJAZXixFMw02A/dZqoIVBA+i12tyIVpNeqqZaEP4e3ctSTucS85nGHJFc5gtlB+vvf7m3g5NhZgC1z1TklioK5j9QJBAM+pkUAjRU8Hhv5uOZyGIYBS4WoRSvRdhjeKFKUsIAElBShie9uPu/lxI9hIy1m3lWCIwmWkdhCTySdfWhk+73o=';
    $saPrivateKey='MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDAigiKGebcQZM/ZH+vm5AeV1EHD3dGJQt5MgulVlYqT0uFFXEBVNmspesCT0fKTPtpdK1u66n1M0FlpgC7Vr5uViwg7LJJ0AtbUbcegK/ajLd14aGiJ5pDHV3uUb24qwo3XlyEZSuHNv6pGdXnSNsEzeQ9GPlMokpWl90wmlQv6LCyk7gaQ3yLUt1x1DNjw3DR6mIFn1O7b6NZcvlkayBuMteY7y3S5ubHrWhmlc3s421TkHcmvX3iiPRyYHca52cuu/SMbsLyDLWeeQDcjKioGIQPLO4jxQOGek5h1CxnqMzkx1VQzPn2DVFXv+9oOfVr4tUjohZDTO0ETZIfPdIvAgMBAAECggEAW6lbJ2PtZC5Ty3osz41frxIioRnbzqkULT6GDNBtdPcLHPB4PAoJFwhmOVNE+ipsBmhlqMjIwNMF7z54WreRMTYVDBejSUCiJ3dflkXtcj5LmPMx3+C6r/jrHuoLSD8lPfbOVLlVX6nu0tJZ4yHsIv1iCzQ4eyNIALGLu/e83JE2QunEXAfyfEaE7oogWDzcelyllBcn1unJqRER2CXFRyHH/dl3BXm63GsZalwlLcaO8OFW195n554yPrPN2ZbXHtDnItlvHevQfkIVL56+RgH0/1MOPzx1UscPFVCuAW61vmVkBZaa2d7HS1CKPuw+/SDjUtB0OA5tGGrLv36hAQKBgQDzgBvaFu2xEeBHkhosDc7VVxANtjNNGgFPfzF6U51F+6c/dQYsE8YBhBLh8P0B/kMu9k8K4U3PGvzhdBr3jePwRIwmudBIDgYT3St5MxbKCuis3K2VPNX3m4hQYlu2jJhAC2N2HVHcLU/6Plf9ZNThtUwh38Yz58LzUpmwfYUfzQKBgQDKbDtq9l5XH7VETme5VbdCiuz4zPRUfycrABu6l5F8FMCCFoHpEwL14u5d2TG+dcAKcmPBZdHiMU945MDM5bjCWwYB6THkxmXTCNSXRUKOOvxrRdxyOIDdDmvOgOqmBNbbxgAT4ur3DVI0W6OiESBNZoH0OCTRvwdwUSVGXzUl6wKBgQDUdxPWV+tOdaqcyH7Nsb6nqC/5xuE8hVRfaTxi4FJ6nfpxBEs+aoJ7ECLpwr0A4Dr+3yxY8f5Sl9nPXt+o4cXh/+3KXJfAR3xGGNW2Cz33TU14h5VugoawZqWqfihIQKUYPNesWCj1VRRT5dIg57pYtziW1gq7f547cfdoaB2ggQKBgQC+wPXRW3AVzDSmtPg8CBGVrJOZ9pJ9GgIQHJ8E3JZmBqxPKuSrfpX4nyyf1fBxmkcp0Rg+hTQfYvaQZ4lkzUqqJNrpbBbvjyMflx+3mbYZkG6UrJYMMUEGj/N5+7SkVMUhAFrMYrenrq771ivCqz70kZf/S/Uuj0V0S5PD2C6elwKBgGvGf/GOs64I1JjxQWG0v3Fn6gOV5vFMv1Y6yzZplin5bDtQ486eiEg/iu2peff/TIOfBXI9ugTvRUVaqVfyQHiMLtcU0LK8TfVKekD88ADe+QdnnxexYJemEzsgkgnS8CcRqd2qGgyE6bXEGeCDnbUDTokmp4OOrxhREGqrB4HY';                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
    $account = $_POST['uname'];      //收款方账户（支付宝登录号，支持邮箱和手机号格式。）
    $realName = $_POST['name'];     //收款方真实姓名
    $aliPay = new AlipayService($appid,$saPrivateKey);
    $result = $aliPay->doPay($payAmount,$outTradeNo,$account,$realName,$remark);

    $result = $result['alipay_fund_trans_toaccount_transfer_response'];
    if($result['code'] && $result['code']=='10000'){
      
        $member=Model('member')->where(['member_id'=>$member_id])->setDec('available_rc_balance',round($_POST['cash'],2));
       if($member){
           $log_pay=Model('recharge_log');
               $seller_member=[
                         'member_id'=>$member_id,
                         'money'=>round($_POST['cash'],2),
                         'type'=>1,
                         'pay_sn'=>time(),
                         'ctime'=>time(),
                           'pay_from'=>4
                     ];
                            $log_pay->insert($seller_member);
       }
        showmessage('转账成功');
    }else{
      showmessage('转账失败');
    }
       

        
    }
    
    public function cash_stateOp() {
        $state_type = $_GET['state_type'];
      

       

      

        if ($_GET['state_type'] == 'cash_cancel') {
            $result = $this->_cash_cancel($order_info,$_POST);
        } 
	
        if (!$result['state']) {
            showDialog($result['msg'],'','error',empty($_GET['inajax']) ?'':'CUR_DIALOG.close();',5);
        } else {
            showDialog($result['msg'],'reload','succ',empty($_GET['inajax']) ?'':'CUR_DIALOG.close();');
        }
        
    }
    
      private function _cash_cancel($post) {
       
            Tpl::output('info',$post);
            Tpl::showpage('store_cash.cancel','null_layout');
            exit();
         
    }
    
    
    
    
    /**
     * 异步取得卖家统计类信息
     *
     */
    public function statisticsOp() {
        $add_time_to = strtotime(date("Y-m-d")+60*60*24);   //当前日期 ,从零点来时
        $add_time_from = strtotime(date("Y-m-d",(strtotime(date("Y-m-d"))-60*60*24*30)));   //30天前
        $goods_online = 0;      // 出售中商品
        $goods_waitverify = 0;  // 等待审核
        $goods_verifyfail = 0;  // 审核失败
        $goods_offline = 0;     // 仓库待上架商品
        $goods_lockup = 0;      // 违规下架商品
        $consult = 0;           // 待回复商品咨询
        $no_payment = 0;        // 待付款
        $no_delivery = 0;       // 待发货
        $no_receipt = 0;        // 待收货
        $refund_lock  = 0;      // 售前退款
        $refund = 0;            // 售后退款
        $return_lock  = 0;      // 售前退货
        $return = 0;            // 售后退货
        $complain = 0;          //进行中投诉

        $model_goods = Model('goods');
        // 全部商品数
        $goodscount = $model_goods->getGoodsCommonCount(array('store_id' => $_SESSION['store_id']));
        // 出售中的商品
        $goods_online = $model_goods->getGoodsCommonOnlineCount(array('store_id' => $_SESSION['store_id']));
        if (C('goods_verify')) {
            // 等待审核的商品
            $goods_waitverify = $model_goods->getGoodsCommonWaitVerifyCount(array('store_id' => $_SESSION['store_id']));
            // 审核失败的商品
            $goods_verifyfail = $model_goods->getGoodsCommonVerifyFailCount(array('store_id' => $_SESSION['store_id']));
        }
        // 仓库待上架的商品
        $goods_offline = $model_goods->getGoodsCommonOfflineCount(array('store_id' => $_SESSION['store_id']));
        // 违规下架的商品
        $goods_lockup = $model_goods->getGoodsCommonLockUpCount(array('store_id' => $_SESSION['store_id']));
        // 等待回复商品咨询
        if (C('dbdriver') == 'mysqli') {
            $consult = Model('consult')->getConsultCount(array('store_id' => $_SESSION['store_id'], 'consult_reply' => ''));
        } else {
            $consult = Model('consult')->getConsultCount(array('store_id' => $_SESSION['store_id'], 'consult_reply' => array('exp', 'consult_reply IS NULL')));
        }

        // 商品图片数量
        $imagecount = Model('album')->getAlbumPicCount(array('store_id' => $_SESSION['store_id']));

        $model_order = Model('order');
        // 交易中的订单
        $progressing = $model_order->getOrderCountByID('store',$_SESSION['store_id'],'TradeCount');
        // 待付款
        $no_payment = $model_order->getOrderCountByID('store',$_SESSION['store_id'],'NewCount');
        // 待发货
        $no_delivery = $model_order->getOrderCountByID('store',$_SESSION['store_id'],'PayCount');

        $model_refund_return = Model('refund_return');
        // 售前退款
        $condition = array();
        $condition['store_id'] = $_SESSION['store_id'];
        $condition['refund_type'] = 1;
        $condition['order_lock'] = 2;
        $condition['refund_state'] = array('lt', 3);
        $refund_lock = $model_refund_return->getRefundReturnCount($condition);
        // 售后退款
        $condition = array();
        $condition['store_id'] = $_SESSION['store_id'];
        $condition['refund_type'] = 1;
        $condition['order_lock'] = 1;
        $condition['refund_state'] = array('lt', 3);
        $refund = $model_refund_return->getRefundReturnCount($condition);
        // 售前退货
        $condition = array();
        $condition['store_id'] = $_SESSION['store_id'];
        $condition['refund_type'] = 2;
        $condition['order_lock'] = 2;
        $condition['refund_state'] = array('lt', 3);
        $return_lock = $model_refund_return->getRefundReturnCount($condition);
        // 售后退货
        $condition = array();
        $condition['store_id'] = $_SESSION['store_id'];
        $condition['refund_type'] = 2;
        $condition['order_lock'] = 1;
        $condition['refund_state'] = array('lt', 3);
        $return = $model_refund_return->getRefundReturnCount($condition);

        $condition = array();
        $condition['accused_id'] = $_SESSION['store_id'];
        $condition['complain_state'] = array(array('gt',10),array('lt',90),'and');
        $complain = Model()->table('complain')->where($condition)->count();

        //待确认的结算账单
        $model_bill = Model('bill');
        $condition = array();
        $condition['ob_store_id'] = $_SESSION['store_id'];
        $condition['ob_state'] = BILL_STATE_CREATE;
        $bill_confirm_count = $model_bill->getOrderBillCount($condition);

        //统计数组
        $statistics = array(
            'goodscount' => $goodscount,
            'online' => $goods_online,
            'waitverify' => $goods_waitverify,
            'verifyfail' => $goods_verifyfail,
            'offline' => $goods_offline,
            'lockup' => $goods_lockup,
            'imagecount' => $imagecount,
            'consult' => $consult,
            'progressing' => $progressing,
            'payment' => $no_payment,
            'delivery' => $no_delivery,
            'refund_lock' => $refund_lock,
            'refund' => $refund,
            'return_lock' => $return_lock,
            'return' => $return,
            'complain' => $complain,
            'bill_confirm' => $bill_confirm_count
        );
        exit(json_encode($statistics));
    }
    /**
     * 添加快捷操作
     */
    function quicklink_addOp() {
        if(!empty($_POST['item'])) {
            $_SESSION['seller_quicklink'][$_POST['item']] = $_POST['item'];
        }
        $this->_update_quicklink();
        echo 'true';
    }

    /**
     * 删除快捷操作
     */
    function quicklink_delOp() {
        if(!empty($_POST['item'])) {
            unset($_SESSION['seller_quicklink'][$_POST['item']]);
        }
        $this->_update_quicklink();
        echo 'true';
    }

    private function _update_quicklink() {
        $quicklink = implode(',', $_SESSION['seller_quicklink']);
        $update_array = array('seller_quicklink' => $quicklink);
        $condition = array('seller_id' => $_SESSION['seller_id']);
        $model_seller = Model('seller');
        $model_seller->editSeller($update_array, $condition);
    }

    /**
     * 从第三方取快递信息
     *
     */
    public function get_expressOp(){

        $content = Model('express')->get_express($_GET['e_code'], $_GET['shipping_code']);
        $output = '';
        foreach ($content as $k=>$v) {
            if ($v['time'] == '') continue;
            $output .= '<li>'.$v['time'].'&nbsp;&nbsp;'.$v['context'].'</li>';
        }
        if ($output == '') exit(json_encode(false));
        echo json_encode($output);
    }
	    /**
     * 分销申请
     *
     */
		public function distributionOp(){
	
		$store_info = $this->store_info;
		$distribution_info = Model('store_distribution')->where(array('distri_store_id'=>$store_info['store_id']))->find();
		if($distribution_info){
			showmessage('您已经提交了申请，请勿重复提交');
		}else{
			$param = array();
			$param['distri_store_id'] = $store_info['store_id'];
			$param['distri_store_name'] = $store_info['store_name'];
			$param['distri_seller_name'] = $store_info['seller_name'];
			$param['distri_create_time'] = time();
			$result = Model('store_distribution')->insert($param);
			if($result){
				showmessage('申请中，请等待审核');
			}else{
				showmessage('申请提交失败');
			}
		}	
	}

}
