<?php
/**
 * 任务计划 - 小时执行的任务
 *
 *
 * 
 */
defined('In33hao') or exit('Access Invalid!');

class monthControl extends BaseCronControl {
     /**
     * 执行频率常量 1个月
     * @var int
     */
    const EXE_TIMES = 2678400;

    private $_doc;
    private $_xs;
    private $_index;
    private $_search;
    private $_contract_item;

    /**
     * 默认方法
     */
    public function indexOp() {

        $this->cash_month();
    }
    
     private function cash_month(){
         $member=Model('member')->where(['seller'=>1])->select();
       
        foreach($member as $k=>$v){
            $invite=Model('invite')->where(['member_id'=>$v['member_id']])->find();
           
            if($invite['invite_id']){
                
                $invite_info=Model('member')->where(['member_id'=>$invite['invite_id']])->find();
           
                $invite_id=$invite_info['member_id'];
                if($invite_info['seller']==1){
                    //获取上个月的时间
                          $month=strtotime("-1 month");
                          $map['member_id']=$v['member_id'];
                          $map['pay_from']=8;
                          $map['ctime']=array('between',$month,time);
                          $map['to_cash']=0;
                    $log=Model('recharge_log')->where($map)->select();
                $all_price='';
                    foreach($log as $key=>$value){
                            $all_price+=$value['money'];
                            Model('recharge_log')->where(['id'=>$value['id']])->update(['to_cash'=>1]);
                    }
                    $cash=round($all_price*0.2,2);
                    $insert=Model('member')->where(['member_id'=>$invite_id])->setInc('available_rc_balance',$cash);
                    
                    if($insert){
                            $data=[
                        'member_id'=>$invite_id,
                        'money'=>$cash,
                        'ctime'=>time(),
                        'type'=>1,
                        'pay_sn'=>10110001,
                        'pay_from'=>9
                    ];
                    $re_log=Model('recharge_log')->insert($data);
                    }
                    
                }
                
            }
        }
     }
}