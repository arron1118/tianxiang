<?php
header('Access-Control-Allow-Origin: *');
header('Content-type: text/plain');

require_once 'aop/AopClient.php';
require_once 'aop/request/AlipayTradeAppPayRequest.php';

// 获取支付金额
$amount='';
if($_SERVER['REQUEST_METHOD']=='POST'){
    $amount=$_POST['total'];
}else{
    $amount=$_GET['total'];
}

$total = floatval($amount);
if(!$total){
    $total = 1;
}

$aop = new AopClient;
$aop->gatewayUrl = "https://openapi.alipay.com/gateway.do";
$aop->appId = "2018041902581911";
$aop->rsaPrivateKey = 'MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAMg/8eQ+PvveCdXUqICy1JZCTiqxekk8cvs5hMr0D1A6vooISzIxSWSBie3n+8B4QTR9B6P1Udamhco4KrzjWwdByVrvsMzPIn//7YAulPKRT8U6PdzoaqlilDLTwpTqj0SK6VM4ag4ecOVD1cjZhG+MGyLT0PdU/XuTwOHBmtGnAgMBAAECgYEApoXefqJwt/x+C06rzVJpTIbY/9HU0kAtkdLUJf3tGhevyZN6DMRFtwmuDKQEi5q1BpQOnX7szmZuDarFh4w9bgLMUN0GXbP5fZPGK6vs/CFRYLExUyrPBlHzmIHjMLLSAC0tD2WDGJSnXfNCkRh0MTvK62/Ux5+SBWhwAIOGOGkCQQD0ixhqnEES0U/rnTxfdJr/mCTYEuHkkRbm25lrA7K5+oKq1SKeNHxVvuikUYoyqDUeH4J6B96PZnmwV8ddW2RTAkEA0aGek5uoErIMh1fQ69aKv/vQHXbwfgzVZtBLjtep96L/6YPDT7/fZP1p7CRKZlTmRuZ/JrENvahiBsjE688y3QJAUBJXfVkKibHVvG1wvkS9F+HmdoXAR4omeJMBKiQ82l6neG5vdmPzLlRjcqJsYOfo9KMWowR+oG/Keq5TTkNFlwJAZXixFMw02A/dZqoIVBA+i12tyIVpNeqqZaEP4e3ctSTucS85nGHJFc5gtlB+vvf7m3g5NhZgC1z1TklioK5j9QJBAM+pkUAjRU8Hhv5uOZyGIYBS4WoRSvRdhjeKFKUsIAElBShie9uPu/lxI9hIy1m3lWCIwmWkdhCTySdfWhk+73o=';
$aop->format = "json";
$aop->charset = "UTF-8";
$aop->signType = "RSA2";
$aop->alipayrsaPublicKey = 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCnxj/9qwVfgoUh/y2W89L6BkRAFljhNhgPdyPuBV64bfQNN1PjbCzkIM6qRdKBoLPXmKKMiFYnkd6rAoprih3/PrQEB/VsW8OoM8fxn67UDYuyBTqA23MML9q1+ilIZwBC2AQ2UBVOrFXfFl75p6/B5KsiNG9zpgmLCUYuLkxpLQIDAQAB';
//实例化具体API对应的request类,类名称和接口名称对应,当前调用接口名称：alipay.trade.app.pay
$request = new AlipayTradeAppPayRequest();

// 异步通知地址
$notify_url = urlencode('http://www.baidu.com');
// 订单标题
$subject = 'DCloud项目捐赠';
// 订单详情
$body = 'DCloud致力于打造HTML5最好的移动开发工具，包括终端的Runtime、云端的服务和IDE，同时提供各项配套的开发者服务。'; 
// 订单号，示例代码使用时间值作为唯一的订单ID号
$out_trade_no = date('YmdHis', time());

//SDK已经封装掉了公共参数，这里只需要传入业务参数
$bizcontent = "{\"body\":\"".$body."\","
                . "\"subject\": \"".$subject."\","
                . "\"out_trade_no\": \"".$out_trade_no."\","
                . "\"timeout_express\": \"30m\","
                . "\"total_amount\": \"".$total."\","
                . "\"product_code\":\"QUICK_MSECURITY_PAY\""
                . "}";
$request->setNotifyUrl($notify_url);
$request->setBizContent($bizcontent);
//这里和普通的接口调用不同，使用的是sdkExecute
$response = $aop->sdkExecute($request);

// 注意：这里不需要使用htmlspecialchars进行转义，直接返回即可
echo $response;
?>
