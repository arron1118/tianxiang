var page = 10;
var curpage = 1;
var hasMore = true;
$(function(){
 if (getQueryString('key') != '') {
        var key = getQueryString('key');
        var username = getQueryString('username');
        addCookie('key', key);
        addCookie('username', username);
    } else {
        var key = getCookie('key');
    }
    if(getQueryString('type') == 'money'){
	 	initPage(key);
	 }else if(getQueryString('type') == 'cash'){
	 	cashpage(key)
	 }else{

   		xqpage(key);
	 }
	 $(window).scroll(function(){
        if(($(window).scrollTop() + $(window).height() > $(document).height()-1)){
        	if(getQueryString('type') == 'money'){
	           initPage(key);
	       }else{
	       		xqpage(key);
       		}
        }
    });
})

function cashpage(key){
	if (!hasMore) {
        //加载完毕
        // $('#loading').hide();
        // $('#nomore').show();
        return false;
    }
    hasMore = false;
		$.ajax({
            type:'post',
            url:ApiUrl+"/index.php?act=member_index&op=member_cash_list&page="+page+"&curpage="+curpage+"",
            data:{key:key},
            dataType:'json',
            //jsonp:'callback',
            success:function(result){
            	curpage++;
                hasMore = result.hasmore;
                if(!result.hasmore){
                    $('#loading').hide();
                    $('#nomore').show();
                }
                for(var i=0;i<result.datas.data.length;i++){
                	//console.log(result.datas.data[i]);
                	if(result.datas.data[i].type == 0){
                		$('#xul').append('<li>\
				              <div class="title">\
				                  购买消耗\
				              </div>\
				              <div class="time">\
				                '+getMyDate(result.datas.data[i].time)+'\
				              </div>\
				              <div class="how cl_b">\
				                -'+result.datas.data[i].cash+'\
				              </div>\
				          </li>');
                	}else if(result.datas.data[i].type == 1){
                		$('#xul').append('<li>\
				              <div class="title">\
				                  成为vip赠送\
				              </div>\
				              <div class="time">\
				                '+getMyDate(result.datas.data[i].time)+'\
				              </div>\
				              <div class="how cl_f">\
				                +'+result.datas.data[i].cash+'\
				              </div>\
				          </li>');
                	}else if(result.datas.data[i].type == 2){
                		$('#xul').append('<li>\
				              <div class="title">\
				                  成为店长赠送\
				              </div>\
				              <div class="time">\
				                '+getMyDate(result.datas.data[i].time)+'\
				              </div>\
				              <div class="how cl_f">\
				                +'+result.datas.data[i].cash+'\
				              </div>\
				          </li>');
                	}else if(result.datas.data[i].type == 3){
                		$('#xul').append('<li>\
				              <div class="title">\
				                  返利\
				              </div>\
				              <div class="time">\
				                '+getMyDate(result.datas.data[i].time)+'\
				              </div>\
				              <div class="how cl_f">\
				                +'+result.datas.data[i].cash+'\
				              </div>\
				          </li>');
                	}else if(result.datas.data[i].type == 10){
                		$('#xul').append('<li>\
				              <div class="title">\
				                  退还\
				              </div>\
				              <div class="time">\
				                '+getMyDate(result.datas.data[i].time)+'\
				              </div>\
				              <div class="how cl_f">\
				                +'+result.datas.data[i].cash+'\
				              </div>\
				          </li>');
                	}
                }
    	 	}
        });
}

//余额
function initPage(key){
	if (!hasMore) {
            //加载完毕
            // $('#loading').hide();
            // $('#nomore').show();
            return false;
        }
        hasMore = false;
	 $.ajax({
            type:'post',
            url:ApiUrl+"/index.php?act=member_index&op=cash_list&page="+page+"&curpage="+curpage+"",
            data:{key:key},
            dataType:'json',
            //jsonp:'callback',
            success:function(result){
            	curpage++;
                hasMore = result.hasmore;
                if(!result.hasmore){
                    $('#loading').hide();
                    $('#nomore').show();
                }
                for(var i=0;i<result.datas.data.length;i++){
                	//console.log(result.datas.data[i]);
                	if(result.datas.data[i].pay_from == 0){
                		$('#xul').append('<li>\
				              <div class="title">\
				                  领取红包\
				              </div>\
				              <div class="time">\
				                '+getMyDate(result.datas.data[i].ctime)+'\
				              </div>\
				              <div class="how cl_f">\
				                +'+result.datas.data[i].money+'\
				              </div>\
				          </li>');
                	}else if(result.datas.data[i].pay_from == 1){
                		$('#xul').append('<li>\
				              <div class="title">\
				                  支付宝充值\
				              </div>\
				              <div class="time">\
				                '+getMyDate(result.datas.data[i].ctime)+'\
				              </div>\
				              <div class="how cl_f">\
				                +'+result.datas.data[i].money+'\
				              </div>\
				          </li>');
                	}else if(result.datas.data[i].pay_from == 2){
                		$('#xul').append('<li>\
				              <div class="title">\
				                  微信充值\
				              </div>\
				              <div class="time">\
				                '+getMyDate(result.datas.data[i].ctime)+'\
				              </div>\
				              <div class="how cl_f">\
				                +'+result.datas.data[i].money+'\
				              </div>\
				          </li>');
                	}else if(result.datas.data[i].pay_from == 3){
                		$('#xul').append('<li>\
				              <div class="title">\
				                  购买商品\
				              </div>\
				              <div class="time">\
				                '+getMyDate(result.datas.data[i].ctime)+'\
				              </div>\
				              <div class="how cl_b">\
				                -'+result.datas.data[i].money+'\
				              </div>\
				          </li>');
                	}else if(result.datas.data[i].pay_from == 4){
                		$('#xul').append('<li>\
				              <div class="title">\
				                  提现\
				              </div>\
				              <div class="time">\
				                '+getMyDate(result.datas.data[i].ctime)+'\
				              </div>\
				              <div class="how cl_b">\
				                -'+result.datas.data[i].money+'\
				              </div>\
				          </li>');
                	}else if(result.datas.data[i].pay_from == 5){
                		$('#xul').append('<li>\
				              <div class="title">\
				                  开通vip赠送\
				              </div>\
				              <div class="time">\
				                '+getMyDate(result.datas.data[i].ctime)+'\
				              </div>\
				              <div class="how cl_f">\
				                +'+result.datas.data[i].money+'\
				              </div>\
				          </li>');
                	}else if(result.datas.data[i].pay_from == 6){
                		$('#xul').append('<li>\
				              <div class="title">\
				                  开通店长赠送\
				              </div>\
				              <div class="time">\
				                '+getMyDate(result.datas.data[i].ctime)+'\
				              </div>\
				              <div class="how cl_f">\
				                +'+result.datas.data[i].money+'\
				              </div>\
				          </li>');
                	}else if(result.datas.data[i].pay_from == 7){
                		$('#xul').append('<li>\
				              <div class="title">\
				                  邀请返利\
				              </div>\
				              <div class="time">\
				                '+getMyDate(result.datas.data[i].ctime)+'\
				              </div>\
				              <div class="how cl_f">\
				                +'+result.datas.data[i].money+'\
				              </div>\
				          </li>');
                	}else if(result.datas.data[i].pay_from == 8){
                		$('#xul').append('<li>\
				              <div class="title">\
				                  店长分红\
				              </div>\
				              <div class="time">\
				                '+getMyDate(result.datas.data[i].ctime)+'\
				              </div>\
				              <div class="how cl_f">\
				                +'+result.datas.data[i].money+'\
				              </div>\
				          </li>');
                	}else if(result.datas.data[i].pay_from == 9){
                		$('#xul').append('<li>\
				              <div class="title">\
				                  推荐店长分红\
				              </div>\
				              <div class="time">\
				                '+getMyDate(result.datas.data[i].ctime)+'\
				              </div>\
				              <div class="how cl_f">\
				                +'+result.datas.data[i].money+'\
				              </div>\
				          </li>');
                	}
                }
            }
        });
}



//香券
function xqpage(key){
	if (!hasMore) {
        //加载完毕
        // $('#loading').hide();
        // $('#nomore').show();
        return false;
    }
    hasMore = false;
		$.ajax({
            type:'post',
            url:ApiUrl+"/index.php?act=member_index&op=coupons_list&page="+page+"&curpage="+curpage+"",
            data:{key:key},
            dataType:'json',
            //jsonp:'callback',
            success:function(result){
            	curpage++;
                hasMore = result.hasmore;
                if(!result.hasmore){
                    $('#loading').hide();
                    $('#nomore').show();
                }
                for(var i=0;i<result.datas.data.length;i++){
                	//console.log(result.datas.data[i]);
                	if(result.datas.data[i].type == 1){
                		$('#xul').append('<li>\
				              <div class="title">\
				                  注册返券\
				              </div>\
				              <div class="time">\
				                '+getMyDate(result.datas.data[i].time)+'\
				              </div>\
				              <div class="how cl_f">\
				                +'+result.datas.data[i].quota+'\
				              </div>\
				          </li>');
                	}else if(result.datas.data[i].type == 2){
                		$('#xul').append('<li>\
				              <div class="title">\
				                  邀请好友\
				              </div>\
				              <div class="time">\
				                '+getMyDate(result.datas.data[i].time)+'\
				              </div>\
				              <div class="how cl_f">\
				                +'+result.datas.data[i].quota+'\
				              </div>\
				          </li>');
                	}else if(result.datas.data[i].type == 3){
                		$('#xul').append('<li>\
				              <div class="title">\
				                  购买返券\
				              </div>\
				              <div class="time">\
				                '+getMyDate(result.datas.data[i].time)+'\
				              </div>\
				              <div class="how cl_f">\
				                +'+result.datas.data[i].quota+'\
				              </div>\
				          </li>');
                	}else if(result.datas.data[i].type == 4){
                		$('#xul').append('<li>\
				              <div class="title">\
				                  购买消耗\
				              </div>\
				              <div class="time">\
				                '+getMyDate(result.datas.data[i].time)+'\
				              </div>\
				              <div class="how cl_b">\
				                -'+result.datas.data[i].quota+'\
				              </div>\
				          </li>');
                	}else if(result.datas.data[i].type == 5){
                		$('#xul').append('<li>\
				              <div class="title">\
				                  注册vip返券\
				              </div>\
				              <div class="time">\
				                '+getMyDate(result.datas.data[i].time)+'\
				              </div>\
				              <div class="how cl_f">\
				                +'+result.datas.data[i].quota+'\
				              </div>\
				          </li>');
                	}else if(result.datas.data[i].type == 6){
                		$('#xul').append('<li>\
				              <div class="title">\
				                  注册店长返券\
				              </div>\
				              <div class="time">\
				                '+getMyDate(result.datas.data[i].time)+'\
				              </div>\
				              <div class="how cl_f">\
				                +'+result.datas.data[i].quota+'\
				              </div>\
				          </li>');
                	}else if(result.datas.data[i].type == 10){
                		$('#xul').append('<li>\
				              <div class="title">\
				                  退还\
				              </div>\
				              <div class="time">\
				                '+getMyDate(result.datas.data[i].time)+'\
				              </div>\
				              <div class="how cl_f">\
				                +'+result.datas.data[i].quota+'\
				              </div>\
				          </li>');
                	}
                }
    	 	}
        });
}