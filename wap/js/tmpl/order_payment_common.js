var key = getCookie('key');
var password,rcb_pay,pd_pay,payment_code,coupons;
var wxChannel = null; // 微信支付
var aliChannel = null; // 支付宝支付
var channel = null; 
var pswsign  = '';
 // 现在支付方式
 function toPay(pay_sn,act,op) {
    $('#pay_snx').text(pay_sn);
    $('#act').text(act); 
     $.ajax({
         type:'post',
         url:ApiUrl+'/index.php?act='+act+'&op='+op,
         data:{
             key:key,
             pay_sn:pay_sn
             },
         dataType:'json',
         success: function(result){
             checkLogin(result.login);
             if (result.datas.error) {
                 $.sDialog({
                     skin:"red",
                     content:result.datas.error,
                     okBtn:false,
                     cancelBtn:false
                 });
                 return false;
             }
             // 从下到上动态显示隐藏内容
             $.animationUp({valve:'',scroll:''});
             
             // if(!result.datas.pay){
             //    window.location.href = WapSiteUrl + '/member/member_paypwd_step1.html?cur=6';
             // }
             if(result.datas.pay_info.pay_amount == 0){
                $('#paytype').text('0');
                $('#payselect_0').addClass('cur');
                $('#payselect_1,#payselect_2').hide()
            }
             // 需要支付金额
             $('#onlineTotal').html(result.datas.pay_info.pay_amount); 
             //$('#total_pay').html(result.datas.pay_info.pay_amount);
              if($('#coupons').attr('checked')){
                var val = ($('#onlineTotal').text()*1) - ($('#xiangquan').text()*1);
                $('#total_pay').text(val.toFixed(2));
               }else{
                    $('#total_pay').text($('#onlineTotal').text())
               }
            //是否拥有香券 
            // if(!result.datas.pay_info.coupons){
            //     $('#isxq').hide();
            // }else{
            //     //香券数量
            //     $('#xiangquan').html(result.datas.pay_info.coupons);
            
            // }

                pswsign = result.datas.pay_info.member_paypwd;
             // 是否设置支付密码
             // if (!result.datas.pay_info.member_paypwd) {
                // setTimeout(" window.location.href = WapSiteUrl + '/tmpl/member/member_paypwd_step1.html?cur=6'",2000);
                // return false;
             // }
           
             // 支付密码标记
             var _use_password = false;
             if (parseFloat(result.datas.pay_info.payed_amount) <= 0) {
                 if (parseFloat(result.datas.pay_info.member_available_pd) == 0 && parseFloat(result.datas.pay_info.member_available_rcb) == 0) {
                     // $('#internalPay').hide();
                 } else {
                     // $('#internalPay').show();
                     // 充值卡
                     if (parseFloat(result.datas.pay_info.member_available_rcb) != 0) {
                         
                         $('#wrapperUseRCBpay').show();
                         $('#availableRcBalance').html(parseFloat(result.datas.pay_info.member_available_rcb).toFixed(2));
                     } else {
                         $('#wrapperUseRCBpay').hide();
                     }
                     
                     // 预存款
                     if (parseFloat(result.datas.pay_info.member_available_pd) != 0) {
                         $('#wrapperUsePDpy').show();
                         $('#availablePredeposit').html(parseFloat(result.datas.pay_info.member_available_pd).toFixed(2));
                     } else {
                         $('#wrapperUsePDpy').hide();
                     }
                 }
             } else {
                 $('#internalPay').hide();
             }
             
            password = '';
            password = $('#paymentPassword').val();
             rcb_pay =1;
             $('#useRCBpay').click(function(){
                 if ($(this).prop('checked')) {
                     _use_password = true;
                     $('#wrapperPaymentPassword').show();
                     rcb_pay = 1;
                 } else {
                     if (pd_pay == 1) {
                         _use_password = true;
                         $('#wrapperPaymentPassword').show();
                     } else {
                         _use_password = false;
                         $('#wrapperPaymentPassword').hide();
                     }
                     rcb_pay = 0;
                 }
             });
             

             pd_pay = 0;
             $('#usePDpy').click(function(){
                 if ($(this).prop('checked')) {
                     _use_password = true;
                     $('#wrapperPaymentPassword').show();
                     pd_pay = 1;
                 } else {
                     if (rcb_pay == 1) {
                         _use_password = true;
                         $('#wrapperPaymentPassword').show();
                     } else {
                         _use_password = false;
                         $('#wrapperPaymentPassword').hide();
                     }
                     pd_pay = 0;
                 }
             });

             payment_code = '';
             if (!$.isEmptyObject(result.datas.pay_info.payment_list)) {
                 var readytoWXPay = false;
                 var readytoAliPay = false;
                 var m = navigator.userAgent.match(/MicroMessenger\/(\d+)\./);
                 if (parseInt(m && m[1] || 0) >= 5) {
                     // 微信内浏览器
                     readytoWXPay = true;
                 } else {
                     readytoAliPay = true;
                 }
                 for (var i=0; i<result.datas.pay_info.payment_list.length; i++) {
                     var _payment_code = result.datas.pay_info.payment_list[i].payment_code;
                     if (_payment_code == 'alipay' && readytoAliPay) {
                         $('#'+ _payment_code).parents('label').show();
                         if (payment_code == '') {
                             payment_code = _payment_code;
                             $('#'+_payment_code).attr('checked', true).parents('label').addClass('checked');
                         }
                     }
                     if (_payment_code == 'wxpay_jsapi' && readytoWXPay) {
                         $('#'+ _payment_code).parents('label').show();
                         if (payment_code == '') {
                             payment_code = _payment_code;
                             $('#'+_payment_code).attr('checked', true).parents('label').addClass('checked');
                         }
                     }
                 }
             }

             $('#alipay').click(function(){
                 payment_code = 'alipay';
             });
             
             $('#wxpay_jsapi').click(function(){
                 payment_code = 'wxpay_jsapi';
             });
             
                $('#useRCBpay').click(function(){
                 payment_code = 'useRCBpay';
             });
             
             $('#toPay').click(function(){
                if($('#coupons').attr('checked')){
                    coupons=$('#xiangquan').text();
                }else{
                    coupons=0;
                }
             });
         }
     });
 }
function show(){  
        if($('#paytype').text() == 2){ //检测是否装有微信
                if ( plus.os.name == "Android" ) { 
                        if(plus.runtime.isApplicationExist({pname:'com.tencent.mm',action:'weixin://'})){
                        }else{
                            plus.nativeUI.toast("检测到您未安装\"微信\"，请到应用市场下载");
                            return false;
                        }
                } else if ( plus.os.name == "iOS" ) {
                        if(plus.runtime.isApplicationExist({pname:'com.tencent.mm',action:'weixin://'})){

                        }else{
                            plus.nativeUI.toast("检测到您未安装\"微信\"，请到appstore下载");
                            return false;
                        }
                }
         }
                if($('#coupons').attr('checked')){
                    coupons=$('#xiangquan').text();
                }else{
                    coupons=0;
                }
                //判断支付类型
               /**
               * 0 为余额
               * 1为支付宝
               * 2为微信
               **/
            if($('#paytype').text()==0){
                    var how = $('#total_pay').text();
                    var now = $('#availableRcBalance').text();
                    if(Number(now)<Number(how)){
                        $.sDialog({
                             skin:"red",
                             content:'余额不足！',
                             okBtn:false,
                             cancelBtn:false
                         });
                         // plus.nativeUI.toast('余额不够，请前往充值或更换其他支付方式');
                        return false;
                    }
                    $('.nctouch-bottom-mask-closes').click();
                    $('#password_closex').show()
                    if(pswsign){ //判断是否设置支付密码
                     /**
                     * init传入参数依次是：正确密码(传空时不对比输入是否正确),密码键盘背景，标题，副标题
                         * */
                        PwdBox.init('','img/pwd_keyboard.png','请输入支付密码','安全支付环境，请放心使用！');
                        /**
                         *res格式：{status:'true或false',password:'用户输入的密码'}
                         *
                         */
                        PwdBox.show(function(res){
                                var psw = res.password
                                $.ajax({
                                     type:'post',
                                     url:ApiUrl+'/index.php?act=member_buy&op=check_pd_pwd',
                                     dataType:'json',
                                     data:{key:key,password:psw},
                                     success:function(result){
                                         if (result.datas.error) {
                                             $.sDialog({
                                                 skin:"red",
                                                 content:result.datas.error,
                                                 okBtn:false,
                                                 cancelBtn:false
                                             });
                                            PwdBox.reset();
                                            return false
                                         }
                                         // console.log($('#pay_snx').text())
                                          goToPayment($('#pay_snx').text(),$('#act').text() == 'member_buy' ? 'pay_new' : 'vr_pay_new',psw);
                                     }
                                 });
                            // if(res.status){
                            //     //重置输入
                            //     alert('密码正确');
                            //     //关闭并重置密码输入
                            //     PwdBox.reset();
                            // }else{
                            //     alert(JSON.stringify(arguments));
                            //     PwdBox.reset();
                            // }
                        });
                    }else{
                        goToPayment($('#pay_snx').text(),$('#act').text() == 'member_buy' ? 'pay_new' : 'vr_pay_new','');
                    }
               }else if($('#paytype').text()==1){
                    Thirdpay($('#pay_snx').text(),$('#act').text() == 'member_buy' ? 'pay_new' : 'vr_pay_new',1)
               }else{
                    Thirdpay($('#pay_snx').text(),$('#act').text() == 'member_buy' ? 'pay_new' : 'vr_pay_new',2)
               }
            
        }
       
 $(function(){
    mui.plusReady(function() {  
        // 获取支付通道
            plus.payment.getChannels(function(channels){
                if ( plus.os.name == "iOS" ){
                    aliChannel=channels[0];
                    wxChannel=channels[1];
                }else if(plus.os.name == "Android"){
                    aliChannel=channels[1];
                    wxChannel=channels[0];
                }
            },function(e){
                alert("获取支付通道失败："+e.message);
            });
    })
    $(document).on('click','.close',function(){
        $('.password-box').hide();
        showaddress()
    })
    $('#Paychoose').click(function(){
               
               if($('#paytype').text()==0){
                    $('.nctouch-bottom-mask-closes').click();
                    show();
                    // $('#keyword_model').show();
                    // $('#buy_step_model2').show();
               }
                // $('.nctouch-bottom-mask-closes').click();
                // $('#keyword_model').show();
                // $('#buy_step_model2').show();
            });
            //输入密码
            $('.xkey').on('click',function(){
            // $(document).on('click','.xkey',function(){
                     var that = $(this);
                        var value  = that.attr('data-value');
                        if($('#cishu').text()<6){
                            $('#cishu').text($('#cishu').text()*1+1);
                         }
                         var cishu  = $('#cishu').text();
                         $('#passwordx'+cishu+'').text(value);
                         $('#xxin'+cishu+'').text('*');
                         if($('#cishu').text() == 6){
                            var mimastr = '';
                            for(var i=1;i<7;i++){
                                mimastr = mimastr+$('#passwordx'+i+'').text();
                            }
                         }
                })
                // $('.xkey').click(function(){
                   
                // });
                //关闭键盘
                $('.keyword-closes').click(function(){
                     $('#keyword_model').hide();
                     //清空密码
                     $('#cishu').text('0');
                     for(var i=1;i<7;i++){
                        $('#passwordx'+i+'').text('');
                         $('#xxin'+i+'').text('');
                     }
                });
                //删除密码
                $('#delete').click(function(){
                    var cishu  = $('#cishu').text();
                    if($('#cishu').text()>0){
                        $('#cishu').text($('#cishu').text()*1-1);
                    }
                    $('#passwordx'+cishu+'').text('');
                    $('#xxin'+cishu+'').text('');
                });

                //选择支付方式
                // $('.payselect').on('click',function(){
                //     var that = $(this);
                //     that.addClass('cur');
                //     that.attr('data-value','1');
                //     that.siblings('.payselect').removeClass('cur');
                //      that.siblings('.payselect').attr('data-value','0');
                // });
                //  选择支付方式
                   // $(".payselect").each(function(i){
                   //      $(this).click(function(){
                   //          $(this).addClass('cur');
                   //          $(this).siblings('.payselect').removeClass('cur');
                   //          $('#paytype').text(i);
                   //      });
                   //  });
                //选择香券改变数值
                // $('#coupons').click(function(){ 
                //    if($(this).attr('checked')){
                //     var val = ($('#onlineTotal').text()*1) - ($('#xiangquan').text()*1)
                //     $('#total_pay').text(val.toFixed(2))
                //    }else{
                //          $('#total_pay').text($('#onlineTotal').text())
                //    }
                // });
 })

function payselect(i){
        $('#payselect_'+i+'').addClass("cur");
        $('#payselect_'+i+'').siblings('.payselect').removeClass('cur'); 
        $('#paytype').text(i);
}
/*
* op 接口名称：pay_new
* key 用户唯一标识
* pay_sn 订单号
* password 支付密码
* rcb_pay 1 为余额支付
* pd_pay 0 充值卡支付
* payment_code：useRCBpay 余额支付 wxpay 微信支付 alipay 支付宝
* coupons 香券使用额度
*/
 function goToPayment(pay_sn,op,password) {
     $.ajax({
             type:'get',
             url:ApiUrl+'/index.php?act=member_payment&op='+op+'&key=' + key + '&pay_sn=' + pay_sn + '&password=' + password + '&rcb_pay=' + rcb_pay + '&pd_pay=' + pd_pay + '&payment_code=useRCBpay&coupons=' + coupons,
             dataType:'json',
             data:{},
             success:function(result){
                 if(result.code == 200){
                    window.location.href = WapSiteUrl+'/tmpl/member/order_list.html?data-state=state_wait&cur=6';
                 }else{
                    plus.nativeUI.toast(result.datas.error);
                    PwdBox.reset();
                 }
             }
         });
      //location.href = ApiUrl+'/index.php?act=member_payment&op='+op+'&key=' + key + '&pay_sn=' + pay_sn + '&password=' + password + '&rcb_pay=' + rcb_pay + '&pd_pay=' + pd_pay + '&payment_code=useRCBpay&coupons=' + coupons;
 }

 //第三方支付
function Thirdpay(pay_sn,op,type){
    var paytype = ''
    if(type==1){
        paytype = 'alipay';
        channel = aliChannel;
    }else{
        paytype = 'wxpay';
        channel = wxChannel;
    }
    var url = ApiUrl+'/index.php?act=member_payment&op='+op+'&key=' + key + '&pay_sn=' + pay_sn + '&password=&rcb_pay=' + rcb_pay + '&pd_pay=' + pd_pay + '&payment_code='+paytype+'&coupons=' + coupons;
   // location.href = ApiUrl+'/index.php?act=member_payment&op='+op+'&key=' + key + '&pay_sn=' + pay_sn + '&password=&rcb_pay=' + rcb_pay + '&pd_pay=' + pd_pay + '&payment_code='+paytype+'&coupons=' + coupons;
   pay(url,channel);
}



  function pay(url,channel){
                // 从服务器请求支付订单
            //     var PAYSERVER='';
            //     if(id=='alipay'){
            //     PAYSERVER=ALIPAYSERVER;
            //     channel = aliChannel;
            // }else if(id=='wxpay'){
            //         PAYSERVER=WXPAYSERVER;
            //         channel = wxChannel;
            //     }else{
            //         plus.nativeUI.alert("不支持此支付通道！",null,"捐赠");
            //         return;
            //  }
            if(!url){
                plus.nativeUI.alert("不支持此支付通道！",null,"捐赠");
                return false;
            }
                var xhr=new XMLHttpRequest();
                xhr.onreadystatechange=function(){
                    switch(xhr.readyState){
                        case 4:
                        if(xhr.status==200){
                            plus.payment.request(channel,xhr.responseText,function(result){
                                plus.nativeUI.alert("支付成功！",function(){
                                window.location.href = WapSiteUrl+'/tmpl/member/order_list.html?data-state=state_wait&cur=6';
                            });
                            },function(error){
                                plus.nativeUI.alert("支付失败"); 
                            });
                        }else{
                            alert("获取订单信息失败！");
                        }
                        break;
                    default:
                    break;
                }
         }
            xhr.open('GET',url);
            xhr.send();
    }