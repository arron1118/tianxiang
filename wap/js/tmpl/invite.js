$(function(){
    if (getQueryString('key') != '') {
        var key = getQueryString('key');
        var username = getQueryString('username');
        addCookie('key', key);
        addCookie('username', username);
    } else {
        var key = getCookie('key');
    }
	if(key){
        $.ajax({
            type:'post',
            url:ApiUrl+"/index.php?act=member_index&op=invite_detail",
            data:{key:key},
            dataType:'json',
            //jsonp:'callback',
            success:function(result){
                checkLogin(result.login);
                var html = ' <img class="headimg" src="' + result.datas.member_info.avatar + '">'
                        + ' <div class="nickname">'+result.datas.member_info.user_name+'</div>'
                       
                        + '  <div class="code_bg">'
                        + '   <div class="code_num">'+result.datas.member_info.invite_code+'</div>'
                        + '<div class="code_txt">专属邀请码</div>'
                        + '<img class="chart" src="'+result.datas.member_info.invite_image+'">'
                        + ' <div class="txt">扫码免费注册成为会员</div>'
                        + '</div>';
                  
                $(".jsto").html(html);


                if(result.datas.member_info.invite_list){
                  var list  = result.datas.member_info.invite_list;
                    for(var i=0;i<list.length;i++){
                        $('#invite_record_list').append('<li class="record_list">\
                          <div class="name">'+list[i].uname+'</div>\
                          <div class="time">'+getMyDate(list[i].ctime)+'</div>\
                          <div class="phone">'+list[i].member_mobile.substring(0, 3) + "****" + list[i].member_mobile.substring(7, 11)+'</div>\
                        </li>');
                    }
                }else{
                  $('.bottom,#invite_record_list').hide();
                }
            }
        });
	} 

	  //滚动header固定到顶部
	  // $.scrollTransparent();
});