var member_id='';
var invite_code = '';
var store_id = getQueryString("store_id");
var member_id = getQueryString("member_id");
var type = getQueryString("type");
var is_sign = getQueryString("is_sign");
var invite_code = getQueryString("invite_code");
var goods_id = getQueryString("goods_id");
function tozc(){
    if(type == "web"){
        window.location.href='register.html?store_id=1&member_id='+member_id+'&type=web&invite_code='+invite_code+'&is_sign=0';
    }else{
        window.location.href="register.html";
    }
}
$(function(){
    var key = getCookie('key');
    if (key) {
        window.location.href = WapSiteUrl+'/tmpl/member/member.html';
        return;
    }

    if(type == "web"){
        $('#one,#header').hide();
        $('.nctouch-main-layout').css('margin-top','0');
        $('#two').css('float','right');
    }
    $.getJSON(ApiUrl + '/index.php?act=connect&op=get_state', function(result){
        var ua = navigator.userAgent.toLowerCase();
        var allow_login = 0;
        if (result.datas.pc_qq == '1') {
            allow_login = 1;
            $('.qq').parent().show();
        }
        if (result.datas.pc_sn == '1') {
            allow_login = 1;
            $('.weibo').parent().show();
        }
        if ((ua.indexOf('micromessenger') > -1) && result.datas.connect_wap_wx == '1') {
            allow_login = 1;
	    $('#connect li').css("width","33.3%");//如果有微信登录插件功能，请把$前面的//去掉即可
            $('.wx').parent().show();
        }
        if (allow_login) {
            $('.joint-login').show();
        }
    });
	var referurl = document.referrer;//上级网址
	$.sValid.init({
        rules:{
            username:"required",
            userpwd:"required"
        },
        messages:{
            username:"手机号码必须填写！",
            userpwd:"密码必填!"
        },
        callback:function (eId,eMsg,eRules){
            if(eId.length >0){
                var errorHtml = "";
                $.map(eMsg,function (idx,item){
                    errorHtml += "<p>"+idx+"</p>";
                });
                plus.nativeUI.toast(errorHtml);
            }else{
                errorTipsHide();
            }
        }  
    });
    var allow_submit = true;
    if(type == 'web'){ //web端登录
        $('#loginbtn').click(function(){
            if (!$(this).parent().hasClass('ok')) {
                return false;
            }
            if (allow_submit) {
                allow_submit = false;
            } else {
                return false;
            }
            var username = $('#username').val();
            var pwd = $('#userpwd').val();
            var client = 'wap';
            if($.sValid()){
                  $.ajax({
                    type:'post',
                    url:ApiUrl+"/index.php?act=login",  
                    data:{username:username,password:pwd,client:client},
                    dataType:'json',
                    success:function(result){
                        allow_submit = true;
                        if(!result.datas.error){
                            if(typeof(result.datas.key)=='undefined'){
                                return false;
                            }else if(!goods_id){
                                location.href = WapSiteUrl+'/tmpl/store.html?store_id=1&member_id='+member_id+'&type=web&invite_code='+invite_code+'&is_sign=1';
                            }else if(goods_id){
                                location.href = WapSiteUrl+'/tmpl/product_detail.html?store_id=1&member_id='+member_id+'&type=web&invite_code='+invite_code+'&is_sign=1&goods_id='+goods_id+'';
                            }
                            errorTipsHide();
                        }else{
                            alert(result.datas.error);
                        }
                    }
                 });  
            }
        });
    }else{
    	$('#loginbtn').click(function(){//会员登陆
            if (!$(this).parent().hasClass('ok')) {
                return false;
            }
            if (allow_submit) {
                allow_submit = false;
            } else {
                return false;
            }
    		var username = $('#username').val();
    		var pwd = $('#userpwd').val();
    		var client = 'wap';
    		if($.sValid()){
    	          $.ajax({
    				type:'post',
    				url:ApiUrl+"/index.php?act=login",	
    				data:{username:username,password:pwd,client:client},
    				dataType:'json',
    				success:function(result){
    				    allow_submit = true;
    					if(!result.datas.error){
    						if(typeof(result.datas.key)=='undefined'){
    							return false;
    						}else{
    						    var expireHours = 8760;
    						    if ($('#checkbox').prop('checked')) {
    						        expireHours = 188;
    						    }
    						    // 更新cookie购物车
    						    updateCookieCart(result.datas.key); 
    							addCookie('username',result.datas.username, expireHours);
    							addCookie('key',result.datas.key, expireHours);
    							location.href = referurl;
    						}
    		                errorTipsHide();
    					}else{
    		                plus.nativeUI.toast(result.datas.error);
    					}
    				}
    			 });  
            }
    	});
	}
	$('.weibo').click(function(){
	    location.href = ApiUrl+'/index.php?act=connect&op=get_sina_oauth2';
	})
    $('.qq').click(function(){
        location.href = ApiUrl+'/index.php?act=connect&op=get_qq_oauth2';
    })
    $('.wx').click(function(){
        location.href = ApiUrl+'/index.php?act=connect&op=index';
    })
});