var goods_id = getQueryString("goods_id");
var page = 10;
var curpage = 1;
var hasMore = true;
function initpage(){
    if (!hasMore) {
            //加载完毕
            // $('#loading').hide();
            // $('#nomore').show();
            return false;
        }
        hasMore = false;
    $.ajax({
        type:'get',
        url:ApiUrl+'/index.php?act=goods&op=goods_evaluate&page='+page+'&curpage='+curpage+'',
        data:{goods_id:goods_id},
        dataType:'json',
        success:function(result){
             if(curpage == 1 && result.datas.goods_eval_list.length==0){
                          //显示缺省页
                        $('.zwpl').show();
                        $('#loading').hide();
                        $('#nomore').hide();
                    }
                    curpage++;
                    hasMore = result.hasmore;
                    if(!result.hasmore){
                        $('#loading').hide();
                        $('#nomore').show();
                    }
                    if(curpage == 2 && result.datas.goods_eval_list.length==0){
                        $('#loading').hide();
                        $('#nomore').hide();
                    }
            var list = result.datas.goods_eval_list;
            for(var i=0;i<list.length;i++){
                $('#pjul').append('<li class="pjli">\
                    <img class="headimg" src="'+list[i].member_avatar+'">\
                    <span class="name">'+list[i].geval_frommembername+'</span>\
                    <div class="desc">\
                        '+list[i].geval_content+'\
                    </div>\
                </li>');
            }
        }
    });
}
$(function(){
    initpage();
    $(window).scroll(function(){
        if(($(window).scrollTop() + $(window).height() > $(document).height()-1)){
            initpage();
        }
    });
//切换tab
    $(document).on('click','.xli',function(){
       var that = $(this);
        that.addClass('active');
        that.siblings().removeClass('active');
       if(that.attr('data-sign') == 1){
            $('#tabone').show();
            $('#tabtwo').hide();
       }else{
            $('#tabone').hide();
            $('#tabtwo').show();
       }
    })
    //渲染list
    // var load_class = new ncScrollLoad();
    // load_class.loadInit({
    //     'url':ApiUrl + '/index.php?act=goods&op=goods_evaluate',
    //     'getparam':{goods_id:goods_id},
    //     'tmplid':'product_ecaluation_script',
    //     'containerobj':$("#product_evaluation_html"),
    //     'iIntervalId':true,
    //     callback:function(){
    //         callback();
    //     } 
    //     });

    // $('#goodsDetail').click(function(){
    //     window.location.href = WapSiteUrl+'/tmpl/product_detail.html?goods_id=' + goods_id;
    // });
    // $('#goodsBody').click(function(){
    //     window.location.href = WapSiteUrl+'/tmpl/product_info.html?goods_id=' + goods_id;
    // });
    // $('#goodsEvaluation').click(function(){
    //     window.location.href = WapSiteUrl+'/tmpl/product_eval_list.html?goods_id=' + goods_id;
    // });
    
//     $('.nctouch-tag-nav').find('a').click(function(){
//         var type = $(this).attr('data-state');
//         load_class.loadInit({
//             url:ApiUrl + '/index.php?act=goods&op=goods_evaluate',
//             getparam:{goods_id:goods_id,type:type},
//             tmplid:'product_ecaluation_script',
//             containerobj:$("#product_evaluation_html"),
//             iIntervalId:true,
//             callback:function(){
//                 callback();
//             }
//             });
//         $(this).parent().addClass('selected').siblings().removeClass('selected');
//     });

});

// function callback(){
//     $('.goods_geval').on('click', 'a', function(){
//         var _this = $(this).parents('.goods_geval');
//         _this.find('.nctouch-bigimg-layout').removeClass('hide');
//         var picBox = _this.find('.pic-box');
//         _this.find('.close').click(function(){
//             _this.find('.nctouch-bigimg-layout').addClass('hide');
//         });
//         if (picBox.find('li').length < 2) {
//             return;
//         }
//         Swipe(picBox[0], {
//             speed: 400,
//             auto: 3000,
//             continuous: false,
//             disableScroll: false,
//             stopPropagation: false,
//             callback: function(index, elem) {
//                 $(elem).parents('.nctouch-bigimg-layout').find('div').last().find('li').eq(index).addClass('cur').siblings().removeClass('cur');
//             },
//             transitionEnd: function(index, elem) {}
//         });
//     });
// }
