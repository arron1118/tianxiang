
var wxChannel = null; // 微信支付
var aliChannel = null; // 支付宝支付
var channel = null; 
$(function(){
    // checkiswx();
	$("[type='radio']").click(function(){
		if($("input[type='radio']:checked").val() == 'wx'){
                // checkiswx();
			$('#wx').addClass('checked');
			$('#zfb').removeClass('checked');
		}else{
			$('#zfb').addClass('checked');
			$('#wx').removeClass('checked');
		}
	});

	$('.li_money').on('click',function(){
		var that = $(this);
		var value  = that.attr('data-value');
		$('#howmoney').val(value)
	});


    mui.plusReady(function() {  
        // 获取支付通道
            plus.payment.getChannels(function(channels){
                if ( plus.os.name == "iOS" ){
                    aliChannel=channels[0];
                    wxChannel=channels[1];
                }else if(plus.os.name == "Android"){
                    aliChannel=channels[1];
                    wxChannel=channels[0];
                }
            },function(e){
                alert("获取充值通道失败："+e.message);
            });
    })

	//下一步拉起支付

	$('#next').click(function(){
//		//支付金额
//		console.log($('#howmoney').val())
//		//wx 为微信 zfb为支付宝
//		console.log($("input[type='radio']:checked").val())
    var money=$('#howmoney').val();
    if(money == 0){
        plus.nativeUI.toast("金额必须大于0");
        return false;
    }
    var type=$("input[type='radio']:checked").val();
     if (getQueryString('key') != '') {
            var key = getQueryString('key');
            var username = getQueryString('username');
            addCookie('key', key);
            addCookie('username', username);
    } else {
            var key = getCookie('key');
    }
	   if(key){
            // $.ajax({
            //     type:'post',
            //     url:ApiUrl+"/index.php?act=member_recharge&op=recharge",
            //     data:{key:key,money:money,type:type},
            //     dataType:'json',
            //     //jsonp:'callback',
            //     success:function(result){
                   
            //     }
            // });
             var paytype = ''
            if(type=="zfb"){
                paytype = 'zfb';
                channel = aliChannel;
            }else{
                paytype = 'wx';
                channel = wxChannel;
            }
           var url = ApiUrl+"/index.php?act=member_recharge&op=recharge&key="+key+"&money="+money+"&type="+paytype+"";
           pay(url,channel);
        }
	});
})


  function pay(url,channel){
            if(!url){
                plus.nativeUI.alert("不支持此支付通道！",null,"捐赠");
                return false;
            }
                var xhr=new XMLHttpRequest();
                xhr.onreadystatechange=function(){
                    switch(xhr.readyState){
                        case 4:
                        if(xhr.status==200){
                            plus.payment.request(channel,xhr.responseText,function(result){
                                plus.nativeUI.alert("充值成功！",function(){
                                window.location.href = WapSiteUrl+'/tmpl/member/money.html';
                            });
                            },function(error){
                                plus.nativeUI.alert("充值失败");
                            });
                        }else{
                            alert("获取订单信息失败！");
                        }
                        break;
                    default:
                    break;
                }
         }
            xhr.open('GET',url);
            xhr.send();
    }


function checkiswx(){
     if ( plus.os.name == "Android" ) { 
        if(plus.runtime.isApplicationExist({pname:'com.tencent.mm',action:'weixin://'})){
        }else{
            plus.nativeUI.toast("检测到您未安装\"微信\"，请到应用市场下载");
            $('#wx_radio').attr('disabled',true);
            return false;
        }
    } else if ( plus.os.name == "iOS" ) {
            if(plus.runtime.isApplicationExist({pname:'com.tencent.mm',action:'weixin://'})){

            }else{
                plus.nativeUI.toast("检测到您未安装\"微信\"，请到appstore下载");
                $('#wx_radio').attr('disabled',true);
               return false;
            }
        }
}
function overFormat(th){
    var v = th.value;
    if(v === ''){
        v = '0.00';
    }else if(v === '0'){
        v = '0.00';
    }else if(v === '0.'){
        v = '0.00';
    }else if(/^0+\d+\.?\d*.*$/.test(v)){
        v = v.replace(/^0+(\d+\.?\d*).*$/, '$1');
        v = inp.getRightPriceFormat(v).val;
    }else if(/^0\.\d$/.test(v)){
        v = v + '0';
    }else if(!/^\d+\.\d{2}$/.test(v)){
        if(/^\d+\.\d{2}.+/.test(v)){
            v = v.replace(/^(\d+\.\d{2}).*$/, '$1');
        }else if(/^\d+$/.test(v)){
            v = v + '.00';
        }else if(/^\d+\.$/.test(v)){
            v = v + '00';
        }else if(/^\d+\.\d$/.test(v)){
            v = v + '0';
        }else if(/^[^\d]+\d+\.?\d*$/.test(v)){
            v = v.replace(/^[^\d]+(\d+\.?\d*)$/, '$1');
        }else if(/\d+/.test(v)){
            v = v.replace(/^[^\d]*(\d+\.?\d*).*$/, '$1');
            ty = false;
        }else if(/^0+\d+\.?\d*$/.test(v)){
            v = v.replace(/^0+(\d+\.?\d*)$/, '$1');
            ty = false;
        }else{
            v = '0.00';
        }
    }
    th.value = v; 
}


function amount(th){
    var regStrs = [
        ['^0(\\d+)$', '$1'], //禁止录入整数部分两位以上，但首位为0
        ['[^\\d\\.]+$', ''], //禁止录入任何非数字和点
        ['\\.(\\d?)\\.+', '.$1'], //禁止录入两个以上的点
        ['^(\\d+\\.\\d{2}).+', '$1'] //禁止录入小数点后两位以上
    ];
    for(i=0; i<regStrs.length; i++){
        var reg = new RegExp(regStrs[i][0]);
        th.value = th.value.replace(reg, regStrs[i][1]);
    }
}