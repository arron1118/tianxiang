var key = getCookie('key');
var goods_id = getQueryString("goods_id");
var map_list = [];
var map_index_id = '';
var store_id;
var xxtop='';
var btnsign  = '';
var isvideo = false;
var store_id = getQueryString("store_id");
var member_id = getQueryString("member_id");
var type = getQueryString("type");
var is_sign = getQueryString("is_sign");
var invite_code = getQueryString("invite_code");
var invite_id = '';
var xinvite_code = '';
var good_name = '';
  var initPhotoSwipeFromDOM = function(gallerySelector) {
        // 解析来自DOM元素幻灯片数据（URL，标题，大小...）
        var parseThumbnailElements = function(el) {
            var thumbElements = el.childNodes,
                numNodes = thumbElements.length,
                items = [],
                figureEl,
                linkEl,
                size,
                item,
                divEl;
            for(var i = 0; i < numNodes; i++) {
                figureEl = thumbElements[i]; // <figure> element
                // 仅包括元素节点
                if(figureEl.nodeType !== 1) {
                    continue;
                }
                divEl = figureEl.children[0];
                linkEl = divEl.children[0]; // <a> element
                size = linkEl.getAttribute('data-size').split('x');
                // 创建幻灯片对象
                item = {
                    src: linkEl.getAttribute('href'),
                    w: parseInt(size[0], 10),
                    h: parseInt(size[1], 10)
                };
                if(figureEl.children.length > 1) {
                    item.title = figureEl.children[1].innerHTML;
                }
                if(linkEl.children.length > 0) {
                    // <img> 缩略图节点, 检索缩略图网址
                    item.msrc = linkEl.children[0].getAttribute('src');
                }
                item.el = figureEl; // 保存链接元素 for getThumbBoundsFn
                items.push(item);
            }
            return items;
        };

        // 查找最近的父节点
        var closest = function closest(el, fn) {
            return el && ( fn(el) ? el : closest(el.parentNode, fn) );
        };

        // 当用户点击缩略图触发
        var onThumbnailsClick = function(e) {
            e = e || window.event;
            e.preventDefault ? e.preventDefault() : e.returnValue = false;
            var eTarget = e.target || e.srcElement;
            var clickedListItem = closest(eTarget, function(el) {
                return (el.tagName && el.tagName.toUpperCase() === 'LI');
            });
            if(!clickedListItem) {
                return;
            }
            var clickedGallery = clickedListItem.parentNode,
                childNodes = clickedListItem.parentNode.childNodes,
                numChildNodes = childNodes.length,
                nodeIndex = 0,
                index;
            for (var i = 0; i < numChildNodes; i++) {
                if(childNodes[i].nodeType !== 1) {
                    continue;
                }
                if(childNodes[i] === clickedListItem) {
                    index = nodeIndex;
                    break;
                }
                nodeIndex++;
            }
            if(index >= 0) {
                openPhotoSwipe( index, clickedGallery );
            }
            return false;
        };

        var photoswipeParseHash = function() {
            var hash = window.location.hash.substring(1),
                params = {};
            if(hash.length < 5) {
                return params;
            }
            var vars = hash.split('&');
            for (var i = 0; i < vars.length; i++) {
                if(!vars[i]) {
                    continue;
                }
                var pair = vars[i].split('=');
                if(pair.length < 2) {
                    continue;
                }
                params[pair[0]] = pair[1];
            }
            if(params.gid) {
                params.gid = parseInt(params.gid, 10);
            }
            return params;
        };

        var openPhotoSwipe = function(index, galleryElement, disableAnimation, fromURL) {
            var pswpElement = document.querySelectorAll('.pswp')[0],
                gallery,
                options,
                items;
            items = parseThumbnailElements(galleryElement);
            // 这里可以定义参数
            options = {
                barsSize: {
                    top: 100,
                    bottom: 100
                },
                fullscreenEl : false,
                tapToClose: true,
                shareButtons: [
                    {id:'wechat', label:'分享微信', url:'#'},
                    {id:'weibo', label:'新浪微博', url:'#'},
                    {id:'download', label:'保存图片', url:'{{raw_image_url}}', download:true}
                ],
                galleryUID: galleryElement.getAttribute('data-pswp-uid'),
                getThumbBoundsFn: function(index) {
                    var thumbnail = items[index].el.getElementsByTagName('img')[0], // find thumbnail
                        pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
                        rect = thumbnail.getBoundingClientRect();
                    return {x:rect.left, y:rect.top + pageYScroll, w:rect.width};
                }
            };
            if(fromURL) {
                if(options.galleryPIDs) {
                    for(var j = 0; j < items.length; j++) {
                        if(items[j].pid == index) {
                            options.index = j;
                            break;
                        }
                    }
                } else {
                    options.index = parseInt(index, 10) - 1;
                }
            } else {
                options.index = parseInt(index, 10);
            }
            if( isNaN(options.index) ) {
                return;
            }
            if(disableAnimation) {
                options.showAnimationDuration = 0;
            }
            gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
            gallery.init();
        };

        var galleryElements = document.querySelectorAll( gallerySelector );
        for(var i = 0, l = galleryElements.length; i < l; i++) {
            galleryElements[i].setAttribute('data-pswp-uid', i+1);
            galleryElements[i].onclick = onThumbnailsClick;
        }
        var hashData = photoswipeParseHash();
        if(hashData.pid && hashData.gid) {
            openPhotoSwipe( hashData.pid ,  galleryElements[ hashData.gid - 1 ], true, true );
        }
    };

$(function (){

  //分享
    document.getElementById("sharex").addEventListener("click", function() {
      if(key){
          if(navigator.userAgent.indexOf("Html5Plus") > -1) {
            // if(key){
              //5+ 原生分享
              window.plusShare({
                  title: good_name,//应用名字
                  content: "香生活一网打尽",
                  href: 'https://shop.tianxiangmall.net/wap/tmpl/product_detail.html?store_id=1&member_id='+invite_id+'&type=web&invite_code='+xinvite_code+'&is_sign=0&goods_id='+goods_id+'',//分享出去后，点击跳转地址
                  thumbs: ["https://shop.tianxiangmall.net/wap/images/thumbs.png"] //分享缩略图
              }, function(result) {
                  //分享回调
              });
            // }else{
            //     window.location.href=WapSiteUrl+'/tmpl/member/login.html';
            // }
          } else {
              alert('请在app打开实现分享');
              //原有wap分享实现 
          }
        }else{
          window.location.href=WapSiteUrl+'/tmpl/member/login.html';
        }
    });

    if(type == 'web'){
        $('#header').hide();
        $('.goods-detail-top').css('top','0');
        $('#product_detail_html').css('margin-top','16rem');
        $('.zg').show();
    }

      initPhotoSwipeFromDOM('.xmy-gallery');
    // $(window).scroll(function(){
    //   if($('#wrapper').attr('data-id')==1){
    //       if(($(window).scrollTop() + $(window).height() > $(document).height()-1)){
    //         xxtop = $(window).scrollTop()
    //         setTimeout(function(){
    //           $('#wrapper').attr('data-id','2')
    //           $('#goodsBody').parents().addClass('cur');
    //           $('#goodsBody').parents().siblings().removeClass('cur');
    //           $('#product_detail_html,#pjjjjj').hide();
    //           $('#fixed-tab-pannel').show();
    //           $(window).scrollTop(1)
    //         }, 400);
    //       }
    //     }
    //      if($('#wrapper').attr('data-id')==2){
    //         if($(window).scrollTop()==0){
    //            setTimeout(function(){
    //               $('#shangp').parents().addClass('cur');
    //               $('#shangp').parents().siblings().removeClass('cur');
    //               $('#product_detail_html').show();
    //               $('#fixed-tab-pannel,#pjjjjj').hide();
    //               $('#wrapper').attr('data-id','1');
    //               if(xxtop){
    //                 var totop = xxtop*1-50;
    //               }else{
    //                 var totop = 0 ;
    //               }
    //               $(window).scrollTop(totop)
    //             }, 400);
    //         }
    //      }
    // });
  //获取详情内容
    var key = getCookie('key');

    var unixTimeToDateString = function(ts, ex) {
        ts = parseFloat(ts) || 0;
        if (ts < 1) {
            return '';
        }
        var d = new Date();
        d.setTime(ts * 1e3);
        var s = '' + d.getFullYear() + '-' + (1 + d.getMonth()) + '-' + d.getDate();
        if (ex) {
            s += ' ' + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
        }
        return s;
    };

    var buyLimitation = function(a, b) {
        a = parseInt(a) || 0;
        b = parseInt(b) || 0;
        var r = 0;
        if (a > 0) {
            r = a;
        }
        if (b > 0 && r > 0 && b < r) {
            r = b;
        }
        return r;
    };

    template.helper('isEmpty', function(o) {
        for (var i in o) {
            return false;
        }
        return true;
    });

     // 图片轮播
    function picSwipe(){
      // var elem = $("#mySwipe")[0];
      // window.mySwipe = Swipe(elem, {
      //   continuous: false,
      //   // disableScroll: true,
      //   stopPropagation: true,
      //   callback: function(index, element) {
      //     $('.goods-detail-turn').find('li').eq(index).addClass('cur').siblings().removeClass('cur');
      //   }
      // });
       var swiper = new Swiper('.swiper-container', {
      // Enable lazy loading
          lazy: true,
          pagination: {
            el: '.swiper-pagination',
            clickable: true,
          },
          navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
          },
          on:{
            slideNextTransitionStart: function(){
               if(isvideo && swiper.activeIndex == 1){
                  exit_video();
               } 
            },
          },
      });

    }
    get_detail(goods_id);
  //点击商品规格，获取新的商品
  function arrowClick(self,myData){
    $(self).addClass("current").siblings().removeClass("current");
    //拼接属性
    var curEle = $(".spec").find("a.current");
    var curSpec = [];
    $.each(curEle,function (i,v){
        // convert to int type then sort
        curSpec.push(parseInt($(v).attr("specs_value_id")) || 0);
    });
    var spec_string = curSpec.sort(function(a, b) { return a - b; }).join("|");
    //获取商品ID
    goods_id = myData.spec_list[spec_string];
    get_detail(goods_id);
  }

  function contains(arr, str) {//检测goods_id是否存入
	    var i = arr.length;
	    while (i--) {
           if (arr[i] === str) {
	           return true;
           }
	    }
	    return false;
	}
  $.sValid.init({
        rules:{
            buynum:"digits"
        },
        messages:{
            buynum:"请输入正确的数字"
        },
        callback:function (eId,eMsg,eRules){
            if(eId.length >0){
                var errorHtml = "";
                $.map(eMsg,function (idx,item){
                    errorHtml += "<p>"+idx+"</p>";
                });
                $.sDialog({
                    skin:"red",
                    content:errorHtml,
                    okBtn:false,
                    cancelBtn:false
                });
            }
        }
    });
  //检测商品数目是否为正整数
  function buyNumer(){
    $.sValid();
  }
  
  function get_detail(goods_id) {
      //渲染页面
      $.ajax({
         url:ApiUrl+"/index.php?act=goods&op=goods_detail",
         type:"get",
         data:{goods_id:goods_id,key:key},
         dataType:"json",
         timeout: 5000,
         beforeSend: function (xhr) {
              // $('.load_wrapper').show();   // 数据加载成功之前，使用loading组件
          },
         success:function(result){
          good_name =result.datas.goods_info.goods_name;
          invite_id = result.datas.invite_id;
          xinvite_code = result.datas.invite_code;
            $('.load_wrapper').hide();
            var data = result.datas;
            if(!data.error){
              //商品图片格式化数据
              if(data.goods_image){
                var goods_image = data.goods_image.split(",");
                data.goods_image = goods_image;
              }else{
                 data.goods_image = [];
              }
              //循环背景图
              $('.xmy-gallery').empty();
              for(var i=0;i<data.goods_image.length;i++){
                // if(i == 0){
                //     $('.my-gallery').append('<span>\
                //       <div class="img-dv">\
                //         <a href="'+data.goods_image[i]+'"  data-size="1080x1080" >\
                //           <i class="bofang" data-src="http://www.w3school.com.cn/i/movie.mp4"></i>\
                //           <div class="xbg_bf"></div>\
                //           <div id="video-box" style="position: absolute;top: 0rem;z-index: -100;height:100%;width:100%;">\
                //              <video style="object-fit: fill;height:100%;width:100%;" controls >\
                //                 <source src="">\
                //               </video>\
                //           </div>\
                //           <img src="'+data.goods_image[i]+'" />\
                //         </a>\
                //       </div>\
                //     </span>');
                // }else{
                  // $('.my-gallery').append('<li>\
                  //     <div class="img-dv">\
                  //       <a href="'+data.goods_image[i]+'"  data-size="1080x1080" >\
                  //         <img src="../images/blank.gif" data-echo="'+data.goods_image[i]+'"  />\
                  //       </a>\
                  //     </div>\
                  //   </li>');
            if(data.goods_info.goods_video){
                isvideo = true;
              if(i == 0){
               $('.xmy-gallery').append(' <span class="swiper-slide">\
                    <div class="img-dv">\
                      <a href="'+data.goods_image[i]+'"  data-size="1080x1080" >\
                      <i class="bofang" data-src="'+data.goods_info.goods_video+'"></i>\
                       <div class="xbg_bf"></div>\
                              <div id="video-box" style="display:none;position: absolute;background:black;top: 0rem;z-index:-100;height:14.5rem;width:100%;">\
                                <video onended="video()" style="object-fit: fill;height:10rem;width:100%;" >\
                                  <source src="">\
                                </video>\
                                <div class="exit_video" onclick="exit_video()">退出播放</div>\
                              </div>\
                        <img data-src="'+data.goods_image[i]+'"  class="swiper-lazy">\
                      </a>\
                      </div>\
                    <div class="swiper-lazy-preloader swiper-lazy-preloader-black"></div>\
                  </span>');
              }else{
                 $('.xmy-gallery').append(' <li class="swiper-slide">\
                    <div class="img-dv">\
                      <a href="'+data.goods_image[i]+'"  data-size="1080x1080" >\
                        <img data-src="'+data.goods_image[i]+'"  class="swiper-lazy">\
                      </a>\
                      </div>\
                    <div class="swiper-lazy-preloader swiper-lazy-preloader-black"></div>\
                  </li>');
              }
            }else{
                 $('.xmy-gallery').append(' <li class="swiper-slide">\
                    <div class="img-dv">\
                      <a href="'+data.goods_image[i]+'"  data-size="1080x1080" >\
                        <img data-src="'+data.goods_image[i]+'"  class="swiper-lazy">\
                      </a>\
                      </div>\
                    <div class="swiper-lazy-preloader swiper-lazy-preloader-black"></div>\
                  </li>');
            }
                // }
                // $('#xli').append('<li class="curli"></li>');
                // $('.goods-detail-turn').find('li').eq(0).addClass('cur');
              }
              //商品规格格式化数据
              if(data.goods_info.spec_name){
                var goods_map_spec = $.map(data.goods_info.spec_name,function (v,i){
                  var goods_specs = {};
                  goods_specs["goods_spec_id"] = i;
                  goods_specs['goods_spec_name']=v;
                  if(data.goods_info.spec_value){
                      $.map(data.goods_info.spec_value,function(vv,vi){
                          if(i == vi){
                            goods_specs['goods_spec_value'] = $.map(vv,function (vvv,vvi){
                              var specs_value = {};
                              specs_value["specs_value_id"] = vvi;
                              specs_value["specs_value_name"] = vvv;
                              return specs_value;
                            });
                          }
                        });
                        return goods_specs;
                  }else{
                      data.goods_info.spec_value = [];
                  }
                });
                data.goods_map_spec = goods_map_spec;
              }else {
                data.goods_map_spec = [];
              }

              //多少评价
              // 虚拟商品限购时间和数量
              if (data.goods_info.is_virtual == '1') {
                  data.goods_info.virtual_indate_str = unixTimeToDateString(data.goods_info.virtual_indate, true);
                  data.goods_info.buyLimitation = buyLimitation(data.goods_info.virtual_limit, data.goods_info.upper_limit);
              }

              // 预售发货时间
              if (data.goods_info.is_presell == '1') {
                  data.goods_info.presell_deliverdate_str = unixTimeToDateString(data.goods_info.presell_deliverdate);
              }

              //渲染模板
              var html = template.render('product_detail', data);
              $("#product_detail_html").html(html);

              if (data.goods_info.is_virtual == '0') {
            	  $('.goods-detail-o2o').remove();
              }
    
              //渲染模板
              var html = template.render('product_detail_sepc', data);
              $("#product_detail_spec_html").html(html);
               if(btnsign == 1){
                  $('#add-cart').show();
                  $('#buy-now').hide();
                }else{
                  $('#add-cart').hide();
                  $('#buy-now').show();
                }
                if(curpage == 2 && !hasMore){
                  $('#loading').hide();
                  $('#nomore').hide();
                  $('.zwpl').show();
                }else if(!hasMore){
                    $('#loading').hide();
                    $('#nomore').show();
                }
              //渲染模板
              // var html = template.render('voucher_script', data);
              // $("#voucher_html").html(html);

              if (data.goods_info.is_virtual == '1') {
            	  store_id = data.store_info.store_id;
            	  virtual();
              }
  
              // 购物车中商品数量
              if (getCookie('cart_count')) {
                  if (getCookie('cart_count') > 0) {
                      $('#cart_count,#cart_count1').html('<sup>'+getCookie('cart_count')+'</sup>');
                  }
              }
               $.ajax({
                  url: ApiUrl + "/index.php?act=goods&op=goods_body",
                  data: {goods_id: goods_id},
                  type: "get",
                  success: function(result) {
                    var str = '';
                   //匹配图片（g表示匹配所有结果i表示区分大小写）
                    var imgReg = /<img.*?(?:>|\/>)/gi;
                    //匹配src属性
                    var srcReg = /src=[\'\"]?([^\'\"]*)[\'\"]?/i;
                    var arr = result.match(imgReg);
                    // alert('所有已成功匹配图片的数组：'+arr);
                    for (var i = 0; i < arr.length; i++) {
                     var src = arr[i].match(srcReg);
                     //获取图片地址
                     if(src[1]){
                        //console.log(src[1]);
                        var xstr = '<p><img src="../images/blank.gif" data-echo="'+src[1]+'"  alt="image"></p>';
                        str = str+xstr;
                      }
                    }
                      $(".fixed-tab-pannel").html(str);
                    lazdimg();
                  }
              });

              //图片轮播
              picSwipe();
              
              //商品描述
              $(".pddcp-arrow").click(function (){
                $(this).parents(".pddcp-one-wp").toggleClass("current");
              });
              //规格属性
              var myData = {};
              myData["spec_list"] = data.spec_list;
              $(".spec a").click(function (){
                var self = this;
                arrowClick(self,myData);
              });
              //购买数量，减
              $(".minus").click(function (){
                 var buynum = $(".buy-num").val();
                 if(buynum >1){
                    $(".buy-num").val(parseInt(buynum-1));
                 }
              });
              //购买数量加
              $(".add").click(function (){
                 var buynum = parseInt($(".buy-num").val());
                 if(buynum < data.goods_info.goods_storage){
                    $(".buy-num").val(parseInt(buynum+1));
                 }
              });
              // 一个F码限制只能购买一件商品 所以限制数量为1
              if (data.goods_info.is_fcode == '1') {
                  $('.minus').hide();
                  $('.add').hide();
                  $(".buy-num").attr('readOnly', true);
              }
              //收藏
              $(".sc_icon").click(function (){
                if(type=='web'&& is_sign==1){
                    window.location.href = 'https://shop.tianxiangmall.net/wap/tmpl/download.html';
                }else if(type=='web'&& is_sign==0){
                    window.location.href=WapSiteUrl+'/tmpl/member/login.html?store_id=1&member_id='+member_id+'&type=web&invite_code='+invite_code+'&is_sign=0&goods_id='+goods_id+'';
                }else{
                  if ($(this).hasClass('s-sc_icon')) {
                      if (dropFavoriteGoods(goods_id)) $('.goods-detail-foot').find('.sc_icon').removeClass('s-sc_icon');$('.goods-option-foot').find('.sc_icon').removeClass('s-sc_icon');
                  } else {
                      if (favoriteGoods(goods_id)) $('.goods-detail-foot').find('.sc_icon').addClass('s-sc_icon');$('.goods-option-foot').find('.sc_icon').addClass('s-sc_icon');
                  }
                }
              });
              //加入购物车
              $("#add-cart").click(function (){
                var key = getCookie('key');//登录标记
                if (!key) {
                    window.location.href = WapSiteUrl+'/tmpl/member/login.html';
                    return false;
                }
                var quantity = parseInt($(".buy-num").val());
                 if(!key){
                     var goods_info = decodeURIComponent(getCookie('goods_cart'));
                     if (goods_info == null) {
                         goods_info = '';
                     }
                     if(goods_id<1){
                         // show_tip();
                         return false;
                     }
                     var cart_count = 0;
                     if(!goods_info){
                         goods_info = goods_id+','+quantity;
                         cart_count = 1;
                     }else{
                         var goodsarr = goods_info.split('|');
                         for (var i=0; i<goodsarr.length; i++) {
                             var arr = goodsarr[i].split(',');
                             if(contains(arr,goods_id)){
                                 // show_tip();
                                 return false;
                             }
                         }
                         goods_info+='|'+goods_id+','+quantity;
                         cart_count = goodsarr.length;
                     }
                     // 加入cookie
                     addCookie('goods_cart',goods_info);
                     // 更新cookie中商品数量
                     addCookie('cart_count',cart_count);
                     // show_tip();
                     getCartCount();
                     $('#cart_count,#cart_count1').html('<sup>'+cart_count+'</sup>');
                     return false;
                 }else{
                    $.ajax({
                       url:ApiUrl+"/index.php?act=member_cart&op=cart_add",
                       data:{key:key,goods_id:goods_id,quantity:quantity},
                       type:"post",
                       success:function (result){
                          var rData = $.parseJSON(result);
                          if(checkLogin(rData.login)){
                            if(!rData.datas.error){
                                // show_tip();
                                // 更新购物车中商品数量
                                delCookie('cart_count');
                                getCartCount();
                                $('#cart_count,#cart_count1').html('<sup>'+getCookie('cart_count')+'</sup>');
                            }else{
                              $.sDialog({
                                skin:"red",
                                content:rData.datas.error,
                                okBtn:false,
                                cancelBtn:false
                              });
                            }
                          }
                       }
                    })
                 }
              });

              //立即购买
              if (data.goods_info.is_virtual == '1') {
                  $("#buy-now").click(function() {
                      var key = getCookie('key');//登录标记
                      if (!key) {
                          window.location.href = WapSiteUrl+'/tmpl/member/login.html';
                          return false;
                      }

                      var buynum = parseInt($('.buy-num').val()) || 0;

                      if (buynum < 1) {
                            $.sDialog({
                                skin:"red",
                                content:'参数错误！',
                                okBtn:false,
                                cancelBtn:false
                            });
                          return;
                      }
                      if (buynum > data.goods_info.goods_storage) {
                            $.sDialog({
                                skin:"red",
                                content:'库存不足！',
                                okBtn:false,
                                cancelBtn:false
                            });
                          return;
                      }

                      // 虚拟商品限购数量
                      if (data.goods_info.buyLimitation > 0 && buynum > data.goods_info.buyLimitation) {
                            $.sDialog({
                                skin:"red",
                                content:'超过限购数量！',
                                okBtn:false,
                                cancelBtn:false
                            });
                          return;
                      }

                      var json = {};
                      json.key = key;
                      json.cart_id = goods_id;
                      json.quantity = buynum;
                      $.ajax({
                          type:'post',
                          url:ApiUrl+'/index.php?act=member_vr_buy&op=buy_step1',
                          data:json,
                          dataType:'json',
                          success:function(result){
                              if (result.datas.error) {
                                  $.sDialog({
                                      skin:"red",
                                      content:result.datas.error,
                                      okBtn:false,
                                      cancelBtn:false
                                  });
                              } else {
                                  location.href = WapSiteUrl+'/tmpl/order/vr_buy_step1.html?goods_id='+goods_id+'&quantity='+buynum;
                              }
                          }
                      });
                  });
              } else {
                  $("#buy-now").click(function (){
                     var key = getCookie('key');//登录标记
                     if(!key){
                        window.location.href = WapSiteUrl+'/tmpl/member/login.html';
                     }else{
                         var buynum = parseInt($('.buy-num').val()) || 0;

                      if (buynum < 1) {
                            $.sDialog({
                                skin:"red",
                                content:'参数错误！',
                                okBtn:false,
                                cancelBtn:false
                            });
                          return;
                      }
                      if (buynum > data.goods_info.goods_storage) {
                            $.sDialog({
                                skin:"red",
                                content:'库存不足！',
                                okBtn:false,
                                cancelBtn:false
                            });
                          return;
                      }

                        var json = {};
                        json.key = key;
                        json.cart_id = goods_id+'|'+buynum;
                        $.ajax({
                            type:'post',
                            url:ApiUrl+'/index.php?act=member_buy&op=buy_step1',
                            data:json,
                            dataType:'json',
                            success:function(result){
                                if (result.datas.error) {
                                    $.sDialog({
                                        skin:"red",
                                        content:result.datas.error,
                                        okBtn:false,
                                        cancelBtn:false
                                    });
                                }else{
                                    location.href = WapSiteUrl+'/tmpl/order/buy_step1.html?goods_id='+goods_id+'&buynum='+buynum;
                                }
                            }
                        });
                     }
                  });

              }

            }else {

              $.sDialog({
                  content: data.error + '！<br>请返回上一页继续操作…',
                  okBtn:false,
                  cancelBtnText:'返回',
                  cancelFn: function() { history.back(); }
              });
            }

            //验证购买数量是不是数字
            $("#buynum").blur(buyNumer);
            

            // 从下到上动态显示隐藏内容
            $.animationUp({
                valve : '.animation-up,#goods_spec_selected',          // 动作触发
                wrapper : '#product_detail_spec_html',    // 动作块
                scroll : '#product_roll',     // 滚动块，为空不触发滚动
                start : function(){       // 开始动作触发事件
                    $('.goods-detail-foot').addClass('hide').removeClass('block');
                },
                close : function(){        // 关闭动作触发事件
                    $('.goods-detail-foot').removeClass('hide').addClass('block');
                }
            });
            
            $.animationUp({
                valve : '#getVoucher',          // 动作触发
                wrapper : '#voucher_html',    // 动作块
                scroll : '#voucher_roll',     // 滚动块，为空不触发滚动
            });

            $('#voucher_html').on('click', '.btn', function(){
                getFreeVoucher($(this).attr('data-tid'));
            });
            
            // 联系客服
            $('.kefu').click(function(){
                window.location.href = WapSiteUrl+'/tmpl/member/chat_info.html?goods_id=' + goods_id + '&t_id=' + result.datas.store_info.member_id;
            })
         },
         error:function(xhr,type){
            $('.load_wrapper').hide();
           $('.fail_show').show();
          }
      });
  }
  
  // $.scrollTransparent(); 
  $('#product_detail_html').on('click', '#get_area_selected', function(){
      $.areaSelected({
          success : function(data){
              $('#get_area_selected_name').html(data.area_info);
              var area_id = data.area_id_2 == 0 ? data.area_id_1:data.area_id_2;
              $.getJSON(ApiUrl + '/index.php?act=goods&op=calc', {goods_id:goods_id,area_id:area_id},function(result){
                  $('#get_area_selected_whether').html(result.datas.if_store_cn);
                  $('#get_area_selected_content').html(result.datas.content);
                  if (!result.datas.if_store) {
                      $('.buy-handle').addClass('no-buy');
                  } else {
                      $('.buy-handle').removeClass('no-buy');
                  }
              });
          }
      });
  });
  
  $('#onload').click(function(){
      window.location.reload();
  });
  $(document).on('click','.bofang',function(){
       var src = $(this).data("src");
       // if(plus.os.name == "iOS"){
         sourceDom = $("<source src=\""+ src +"\">");
         $("#video-box video").append(sourceDom);
         setTimeout(function(){
            if(checkmobile() == 'android'){
           $("#video-box").css('z-index','100');
         }
         // 自动播放
         $("#video-box").show();
         $("#video-box video")[0].play();
         },1500)
       // }else{
       //    var Intent = plus.android.importClass("android.content.Intent");
       //    var Uri = plus.android.importClass("android.net.Uri");
       //    var main = plus.android.runtimeMainActivity();
       //    var intent=new Intent(Intent.ACTION_VIEW);
       //    var uri=Uri.parse(src);
       //    intent.setDataAndType(uri,"video/*");
       //    main.startActivity(intent);
       // }
    // $("#video-box video")[0].requestFullscreen();
  })

  // $('body').on('click', '#goodsBody,#goodsBody1', function(){
  //     window.location.href = WapSiteUrl+'/tmpl/product_info.html?goods_id=' + goods_id;
  // });
  // $('body').on('click', '#goodsEvaluation,#goodsEvaluation1', function(){
  //     window.location.href = WapSiteUrl+'/tmpl/product_eval_list.html?goods_id=' + goods_id;
  // });

  //tab
    $('body').on('click', '#goodsBody,#goodsBody1', function(){ 
      $('#goodsBody').parents().addClass('cur');
      $('#goodsBody').parents().siblings().removeClass('cur');
        $('#product_detail_html,#pjjjjj,.goods-detail-top').hide();
        $('#fixed-tab-pannel').show();
        $('#wrapper').attr('data-id','2');
        $(window).scrollTop(1);
    });
    $('body').on('click', '#goodsEvaluation,#goodsEvaluation1', function(){ 
        $('#goodsEvaluation').parents().addClass('cur');
        $('#goodsEvaluation').parents().siblings().removeClass('cur');
        $('#pjjjjj').show();
        $('#fixed-tab-pannel,#product_detail_html,.goods-detail-top').hide();
        $('#wrapper').attr('data-id','3');
        $(window).scrollTop(1);
    });
    $('body').on('click', '#shangp', function(){ 
        $('#shangp').parents().addClass('cur');
        $('#shangp').parents().siblings().removeClass('cur');
        $('#product_detail_html,.goods-detail-top').show();
        $('#fixed-tab-pannel,#pjjjjj').hide();
        $('#wrapper').attr('data-id','1');
        $(window).scrollTop(0);
    });
  $('#list-address-scroll').on('click','dl > a',map);
  $('#map_all').on('click',map);
});


function show_tip() {
    var flyer = $('.goods-pic > img').clone().css({'z-index':'999','height':'3rem','width':'3rem'});
    flyer.fly({
        start: {
            left: $('.goods-pic > img').offset().left,
            top: $('.goods-pic > img').offset().top-$(window).scrollTop()
        },
        end: {
            left: $("#cart_count1").offset().left+40,
            top: $("#cart_count1").offset().top-$(window).scrollTop(),
            width: 0,
            height: 0
        },
        onEnd: function(){
            flyer.remove();
        }
    });
}

function virtual() {
	$('#get_area_selected').parents('.goods-detail-item').remove();
    $.getJSON(ApiUrl + '/index.php?act=goods&op=store_o2o_addr', {store_id:store_id},function(result){
    	if (!result.datas.error) {
    		if (result.datas.addr_list.length > 0) {
    	    	// $('#list-address-ul').html(template.render('list-address-script',result.datas));
    	    	map_list = result.datas.addr_list;
    	    	var _html = '';
    	    	_html += '<dl index_id="0">';
    	    	_html += '<dt>'+ map_list[0].name_info +'</dt>';
    	    	_html += '<dd>'+ map_list[0].address_info +'</dd>';
    	    	_html += '</dl>';
    	    	_html += '<p><a href="tel:'+ map_list[0].phone_info +'"></a></p>';
    	    	$('#goods-detail-o2o').html(_html);

    	    	$('#goods-detail-o2o').on('click','dl',map);

    	    	if (map_list.length > 1) {
    	    		$('#store_addr_list').html('查看全部'+map_list.length+'家分店地址');
    	    	} else {
    	    		$('#store_addr_list').html('查看商家地址');
    	    	}
    	    	$('#map_all > em').html(map_list.length);    			
    		} else {
    			$('.goods-detail-o2o').hide();
    		}
    	}
    });
    $.animationLeft({
        valve : '#store_addr_list',
        wrapper : '#list-address-wrapper',
        scroll : '#list-address-scroll'
    });
}

function map() {
	  $('#map-wrappers').removeClass('hide').removeClass('right').addClass('left');
	  $('#map-wrappers').on('click', '.header-l > a', function(){
		  $('#map-wrappers').addClass('right').removeClass('left');
	  });
	  $('#baidu_map').css('width', document.body.clientWidth);
	  $('#baidu_map').css('height', document.body.clientHeight);
	  map_index_id = $(this).attr('index_id');
	  if (typeof map_index_id != 'string'){
		  map_index_id = '';
	  }
	  if (typeof(map_js_flag) == 'undefined') {
	      $.ajax({
	          url: WapSiteUrl+'/js/map.js',
	          dataType: "script",
	          async: false
	      });
	  }
	if (typeof BMap == 'object') {
	    baidu_init();
	} else {
	    load_script();
	}
}

function tomodel(a){
  if(type == 'web' ){
      return false;
    }else{
        btnsign= a;
      if(a == 1){
        $('#add-cart').show();
        $('#buy-now').hide();
      }else{
        $('#add-cart').hide();
        $('#buy-now').show();
      }
  }
}

function lazdimg(){
                Echo.init({

  offset: 0,//离可视区域多少像素的图片可以被加载

  throttle: 0//图片延时多少毫秒加载

});
}
function video(){
  $('#video-box').hide();
  $("#video-box").css('z-index','-100');
}
function exit_video(){
  $('#video-box').hide();
  $("#video-box").css('z-index','-100');
  $("#video-box video")[0].pause();
}


function checkmobile(){
  var browser = {
    versions: function() {
    var u = navigator.userAgent, app = navigator.appVersion;
    return {
    trident: u.indexOf('Trident') > -1,
    presto: u.indexOf('Presto') > -1,
    webKit: u.indexOf('AppleWebKit') > -1,
    gecko: u.indexOf('Gecko') > -1 && u.indexOf('KHTML') == -1,
    mobile: !!u.match(/AppleWebKit.*Mobile.*/) || !!u.match(/AppleWebKit/),
    ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/),
    android: u.indexOf('Android') > -1 || u.indexOf('Linux') > -1,
    iPhone: u.indexOf('iPhone') > -1 || u.indexOf('Mac') > -1,
    iPad: u.indexOf('iPad') > -1,
    webApp: u.indexOf('Safari') == -1
    };
    }(),
    language: (navigator.browserLanguage || navigator.language).toLowerCase()
  }
  if (browser.versions.ios || browser.versions.iPhone || browser.versions.iPad) {
    return 'ios';
  }
  if (browser.versions.android) {
    return 'android';
  }
}


function tospxq(id){
    if(type=='web' && is_sign==1 ){
        window.location.href=WapSiteUrl+"/tmpl/product_detail.html?goods_id="+id+"&store_id=1&member_id="+member_id+"&type=web&invite_code="+invite_code+"&is_sign=1"
    }else if(type=='web' && is_sign==0){
        window.location.href=WapSiteUrl+"/tmpl/product_detail.html?goods_id="+id+"&store_id=1&member_id="+member_id+"&type=web&invite_code="+invite_code+"&is_sign=0"
    }else{
        window.location.href=WapSiteUrl+"/tmpl/product_detail.html?goods_id="+id+""
    }
}