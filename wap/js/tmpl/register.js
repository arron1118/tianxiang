var member_id='';
var invite_code = '';
var store_id = getQueryString("store_id");
var member_id = getQueryString("member_id");
var type = getQueryString("type");
var is_sign = getQueryString("is_sign");
var invite_code = getQueryString("invite_code");
var invite_rcode = getQueryString('invite_rcode');
$(function(){
    var key = getCookie('key');
    if(invite_rcode){
        $('#invite').val(invite_rcode);
    }
    if(type=='web'){
        $('#header').hide();
        $('.nctouch-main-layout').css('margin-top','0');
         $('#invite').val(invite_code);
    }
    if (key) {
        window.location.href = WapSiteUrl+'/tmpl/member/member.html';
        return;
    }
    $.getJSON(ApiUrl + '/index.php?act=connect&op=get_state&t=connect_sms_reg', function(result){
        if (result.datas != '0') {
            $('.register-tab').show();
        }
    });
    if(type=='web'){
        $('#get_phy').click(function(){
     	  var phone = $("input[name=phone]").val();
            if(!phone){
                alert('请填写手机号码!');
                return false;
            }
            // 0为可以发送验证码 1为还在倒计时中
            if($(this).attr('data-value') == 0){
                 $.ajax({
                    type:'post',
                    url:ApiUrl+"/index.php?act=connect&op=get_sms_captcha", 
                    data:{phone:phone,type:1},
                    dataType:'json',
                    success:function(result){
                           if(result.code !=200){
                                alert(result.datas.error);
                           }else{
                                settime();
                           }
                        }
                    });
            }
        });
    }else{
         $('#get_phy').click(function(){
          var phone = $("input[name=phone]").val();
            if(!phone){
                plus.nativeUI.toast('请填写手机号码!');
                return false;
            }
            // 0为可以发送验证码 1为还在倒计时中
            if($(this).attr('data-value') == 0){
                 $.ajax({
                    type:'post',
                    url:ApiUrl+"/index.php?act=connect&op=get_sms_captcha", 
                    data:{phone:phone,type:1},
                    dataType:'json',
                    success:function(result){
                           if(result.code !=200){
                                plus.nativeUI.toast(result.datas.error);
                           }else{
                                settime();
                           }
                        }
                    });
            }
        });
    }
	$.sValid.init({//注册验证
        rules:{
        	username:"required",
            userpwd:"required",            
            password_confirm:"required",
            email:{
            	required:true,
            	email:true
            }
        },
        messages:{
            username:"用户名必须填写！",
            userpwd:"密码必填!", 
            password_confirm:"确认密码必填!",
            email:{
            	required:"邮件必填!",
            	email:"邮件格式不正确"
            }
        },
        callback:function (eId,eMsg,eRules){
            if(eId.length >0){
                var errorHtml = "";
                $.map(eMsg,function (idx,item){
                    errorHtml += "<p>"+idx+"</p>";
                });
                plus.nativeUI.toast(errorHtml);
            }else{
                //errorTipsHide();
            }
        }  
    });
	
    if(type == 'web'){
        $('#registerbtn').click(function(){
            if (!$(this).parent().hasClass('ok')) {
                return false;
            }
            var phy = $("input[name=phy]").val();
            var phone = $("input[name=phone]").val(); 
           if($("#phone").val().length == 11 && $("#phy").val().length == 6){
              $.getJSON(ApiUrl + '/index.php?act=connect&op=check_sms_captcha', {type:1,phone:phone,captcha:phy }, function(result){
            if (!result.datas.error) {
              return true;
            } else {
                loadSeccode();
               alert(result.datas.error);
            }
        });
            }
            var username = $("input[name=username]").val();
            var pwd = $("input[name=pwd]").val();
            var password_confirm = $("input[name=password_confirm]").val();
            var email = $("input[name=email]").val(); 
            var invite = $("input[name=invite]").val();
            var client = 'wap';
            //三个必填项
            if(!phone){
                alert('请填写手机号码!');
                return false;
            }
            if(!phy){
                alert('请填写验证码!');
                return false;
            }
            if(!pwd){
                alert('请填写密码!');
                return false;
            }
            if(!$('#checkbox').attr('checked')){
               alert('只有阅读并接受协议才能注册!');
                return false;
            }
            if($.sValid()){
                $.ajax({
                    type:'post',
                    url:ApiUrl+"/index.php?act=login&op=register",  
                    data:{invite:invite,phone1:phone,phy:phy,username:username,password:pwd,password_confirm:password_confirm,email:email,client:client},
                    dataType:'json',
                    success:function(result){
                        if(!result.datas.error){
                            if(typeof(result.datas.key)=='undefined'){
                                return false;
                            }else{
                                // 更新cookie购物车
                                location.href = 'https://shop.tianxiangmall.net/wap/tmpl/download.html';
                            }
                            errorTipsHide();
                        }else{
                            alert(result.datas.error);
                        }
                    }
                });         
            }
        });
    }else{
    	$('#registerbtn').click(function(){
            if (!$(this).parent().hasClass('ok')) {
                return false;
            }
            var phy = $("input[name=phy]").val();
            var phone = $("input[name=phone]").val(); 
           if($("#phone").val().length == 11 && $("#phy").val().length == 6){
              $.getJSON(ApiUrl + '/index.php?act=connect&op=check_sms_captcha', {type:1,phone:phone,captcha:phy }, function(result){
            if (!result.datas.error) {
              return true;
            } else {
                loadSeccode();
                plus.nativeUI.toast(result.datas.error);
            }
        });
        	}
    		var username = $("input[name=username]").val();
    		var pwd = $("input[name=pwd]").val();
    		var password_confirm = $("input[name=password_confirm]").val();
    		var email = $("input[name=email]").val(); 
            var invite = $("input[name=invite]").val();
            var client = 'wap';
            //三个必填项
    		if(!phone){
                plus.nativeUI.toast('请填写手机号码!');
                return false;
            }
            if(!phy){
                plus.nativeUI.toast('请填写验证码!');
                return false;
            }
            if(!pwd){
                plus.nativeUI.toast('请填写密码!');
                return false;
            }
            if(!$('#checkbox').attr('checked')){
                plus.nativeUI.toast('只有阅读并接受协议才能注册!');
                return false;
            }
    		if($.sValid()){
    			$.ajax({
    				type:'post',
    				url:ApiUrl+"/index.php?act=login&op=register",	
    				data:{invite:invite,phone1:phone,phy:phy,username:username,password:pwd,password_confirm:password_confirm,email:email,client:client},
    				dataType:'json',
    				success:function(result){
    					if(!result.datas.error){
    						if(typeof(result.datas.key)=='undefined'){
    							return false;
    						}else{
                                // 更新cookie购物车
                                updateCookieCart(result.datas.key);
    							addCookie('username',result.datas.username);
    							addCookie('key',result.datas.key);
    							location.href = WapSiteUrl+'/tmpl/member/member.html?cur=4';
    						}
    		                errorTipsHide();
    					}else{
    		                plus.nativeUI.toast(result.datas.error);
    					}
    				}
    			});			
    		}
    	});
    }
});


//倒计时
var countdown=60;
var t
function settime(val) {
     t = setTimeout(function() { 
        settime() 
    },1000) 
    if (countdown == 0) { 
        clearTimeout(t);
        $('#get_phy').text('获取验证码');
        $('#get_phy').css({
            'background':'#e5ac52',
            'color':'white'
         });
         $('#get_phy').attr('data-value','0');
        countdown = 60; 
    } else { 
        countdown--;
        $('#get_phy').text(countdown+'s后重发');
         $('#get_phy').css({
            'background':'#eee',
            'color':'#555'
         });
         $('#get_phy').attr('data-value','1');
    } 
}