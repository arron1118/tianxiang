$(function(){
    if (getQueryString('key') != '') {
        var key = getQueryString('key');
        var username = getQueryString('username');
        addCookie('key', key);
        addCookie('username', username);
    } else {
        var key = getCookie('key');
    }
	if(key){
        $.ajax({
            type:'post',
            url:ApiUrl+"/index.php?act=member_index",
            data:{key:key},
            dataType:'json',
            //jsonp:'callback',
            success:function(result){
                checkLogin(result.login);
                var Idenurl
                var idenstr
                if(result.datas.member_info.u_level == 0){
                    // Idenurl = '../../images/bj_member.png';
                    idenstr = ''
                }else if(result.datas.member_info.u_level == 1){
                    // Idenurl = '../../images/vip_member.png';
                    idenstr = '<img class="iden" src="../../images/vip_member.png">'
                }else if(result.datas.member_info.u_level == 2){
                    // Idenurl = '../../images/dianzhu.png';
                    idenstr = '<img class="iden" src="../../images/dianzhu.png">'
                }
                var html =' <div class="headimg"><img onclick="toinformation()" src="' + result.datas.member_info.avatar + '"></div> '
              + idenstr
          + '<div class="name" onclick="toinformation()"><span>'+result.datas.member_info.user_name+'</span></div>'
         + '<div class="head_three">'
           + '<a href="consumer_coupon.html">'
           + '<div class="head_three_list">'
           + '<div class="ft_one">'+result.datas.member_info.cash_flow+'</div>'
            + '<div class="ft_two">优惠券</div>'
           + '</div>'
            + '</a>'
            + '<a href="member_cash.html">'
            + '<div class="head_three_list">'
             + '<div class="ft_one bdr_l">'+result.datas.member_info.member_cash+'</div>'
             + '<div class="ft_two">代金券</div>'
           + '</div>'
          + '</a>'
           + '<a href="money.html">'
            + '<div class="head_three_list">'
             + '<div class="ft_one bdr_l">'+result.datas.member_info.money+'</div>'
             + '<div class="ft_two">余额</div>'
           + '</div>'
          + '</a>'
         + '</div>';
                   
                //渲染页面
                
                $(".member_head").html(html);
                
                if(result.datas.member_info.is_shop == 1){
                    $('#xdianpu').show()
                }
                
                var html = ' <img class="headimg" src="' + result.datas.member_info.avatar + '">'
                        + ' <div class="nickname">'+result.datas.member_info.user_name+'</div>'
                        // + ' <div class="duihua">'
                        // + '  <div class="duihua_txt">邀请您成为商城的会员享受购物折扣</div>'
                        // + '  </div>'
                        + '  <div class="code_bg">'
                        + '   <div class="code_num">'+result.datas.member_info.invite_code+'</div>'
                        + '<div class="code_txt">专属邀请码</div>'
                        + '<img class="chart" src="../../images/chart.png">'
                        + ' <div class="txt">扫码免费注册成为会员</div>'
                        + '</div>';
                  
                $(".jsto").html(html);
                
                var html = '<li><a href="order_list.html?data-state=state_new">'+ (result.datas.member_info.order_nopay_count > 0 ? '<em></em>' : '') +'<i class="cc-01"></i><p>待付款</p></a></li>'
                    + '<li><a href="order_list.html?data-state=state_send">' + (result.datas.member_info.order_noreceipt_count > 0 ? '<em></em>' : '') + '<i class="cc-02"></i><p>待收货</p></a></li>'
                    + '<li><a href="order_list.html?data-state=state_notakes">' + (result.datas.member_info.order_notakes_count > 0 ? '<em></em>' : '') + '<i class="cc-03"></i><p>待自提</p></a></li>'
                    + '<li><a href="order_list.html?data-state=state_noeval">' + (result.datas.member_info.order_noeval_count > 0 ? '<em></em>' : '') + '<i class="cc-04"></i><p>待评价</p></a></li>'
                    + '<li><a href="member_refund.html">' + (result.datas.member_info.return > 0 ? '<em></em>' : '') + '<i class="cc-05"></i><p>退款/退货</p></a></li>';
                //渲染页面
                
                $("#order_ul").html(html);
                
                var html = '<li><a href="predepositlog_list.html"><i class="cc-06"></i><p>预存款</p></a></li>'
                    + '<li><a href="rechargecardlog_list.html"><i class="cc-07"></i><p>充值卡</p></a></li>'
                    + '<li><a href="voucher_list.html"><i class="cc-08"></i><p>代金券</p></a></li>'
                    + '<li><a href="redpacket_list.html"><i class="cc-09"></i><p>红包</p></a></li>'
                    + '<li><a href="pointslog_list.html"><i class="cc-10"></i><p>积分</p></a></li>';
                $('#asset_ul').html(html);
                return false;
            }
        });
	} else {
	     var html = ' <a href="login.html" style="display:block">'
        + ' <div class="headimg"><img src="../../images/headimg.png"></div>'
           + '<div class="name">未登录</div>'
           + '</a>'
          + '<div class="head_three">'
           + '  <div class="head_three_list">'
              + '   <div class="ft_one">0.00</div>'
              + '   <div class="ft_two">优惠券</div>'
             + '</div>'
            + '<div class="head_three_list">'
             + '<div class="ft_one bdr_l">0.00</div>'
             + '<div class="ft_two">代金券</div>'
           + '</div>'
            + ' <div class="head_three_list">'
              + '    <div class="ft_one bdr_l">0.00</div>'
               + '  <div class="ft_two">余额</div>'
            + ' </div>'
           + '</div>';
                   
                //渲染页面
                
                $(".member_head").html(html);
	    
      
   
        return false;
	}

	  //滚动header固定到顶部
	  // $.scrollTransparent();

});

function toinformation(){
    window.location.href=WapSiteUrl+'/tmpl/member/information.html'
}