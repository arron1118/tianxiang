var username = '';
$(function(){
	 if (getQueryString('key') != '') {
        var key = getQueryString('key');
        var username = getQueryString('username');
        addCookie('key', key);
        addCookie('username', username);
    } else {
        var key = getCookie('key');
    }
    $.ajax({
    	type:'post',
    	url:ApiUrl+"/index.php?act=member_index",
        data:{key:key},
        dataType:'json',
        success:function(result){
        	$('#touxiang').attr('src',result.datas.member_info.avatar);
        	$('#name').text(result.datas.member_info.user_name);
        	username = result.datas.member_info.user_name
        }
    });
    document.getElementById("promptBtn").addEventListener('tap', function(e) {
		e.detail.gesture.preventDefault(); //修复iOS 8.x平台存在的bug，使用plus.nativeUI.prompt会造成输入法闪一下又没了
		var btnArray = ['确定', '取消'];
		mui.prompt('请输入你要修改的用户名：', username, '修改用户名', btnArray, function(e) {
			if (e.index == 0) {
				 $.ajax({
			    	type:'post',
			    	url:ApiUrl+"/index.php?act=member_index&op=member_name",
			        data:{username:e.value,key:key},
			        dataType:'json',
			        success:function(result){
			        	mui.toast('修改成功!');
			        	$('#name').text(result.datas.data.username);
			        }
			    });
				// info.innerText = '谢谢你的评语：' + e.value;
			} else {
				// info.innerText = '你点了取消按钮';
			}
		})
	});
	$('#file').change(function(){
//		 var file = this.files[0];
//		 if(file.type.indexOf("image")!=0){
//		 	mui.toast('请上传图片格式文件!');
//		 	return false;
//		 }
//		 console.log(file)
//		 　　//formdata储存异步上传数据
    var formData = new FormData($('form')[0]);
//        formData.append('file',$(':file')[0].files[0]);
		 //上传图片
		 $.ajax({
		type:'post',
                 data: formData,
                //这两个设置项必填
                contentType: false,
                processData: false,
	    	url:ApiUrl+"/index.php?act=index&op=upload_touxiang",
//	        data:{file:file.name,key:key},
	        dataType:'json',
	        success:function(result){
	        	//上传成功返回图片地址
	        	$('#touxiang').attr('src',result.datas);
	        	mui.toast('修改成功!');
	        }
		 })
	});
})