$(function() {
    var myScroll;
    $("#header").on('click', '.xsearch', function(){
        location.href = WapSiteUrl + '/tmpl/search.html?cur=6';
    });
    $.getJSON(ApiUrl+"/index.php?act=goods_class", function(result){
		var data = result.datas;
       if(getCookie('tabsign')){
            initPage(data.class_list[getCookie('tabsign')].gc_id);
            data["tabsign"] = getCookie('tabsign');
        }else{
            initPage(data.class_list[0].gc_id);
            data["tabsign"] = 0;
        }
		data.WapSiteUrl = WapSiteUrl;
		var html = template.render('category-one', data);
		$("#categroy-cnt").html(html);
		myScroll = new IScroll('#categroy-cnt', { mouseWheel: true, click: true });
	});
	
	// get_brand_recommend();
	
	$('#categroy-cnt').on('click','.category', function(){
	    // $('.pre-loading').show();
	    $(this).parent().addClass('selected').siblings().removeClass("selected");
	    var gc_id = $(this).attr('date-id');
        // console.log($(this).attr('data-index'))
        addCookie('tabsign',$(this).attr('data-index'));
	    $.getJSON(ApiUrl + '/index.php?act=goods_class&op=get_child_all', {gc_id:gc_id}, function(result){
	        var data = result.datas;
            // alert(data.class_list[0].gc_name)
            data.WapSiteUrl = WapSiteUrl;
            var html = template.render('category-two', data);
            $("#categroy-rgt").html(html);
            // $('.pre-loading').hide();
            // alert('1111')
            new IScroll('#categroy-rgt', { mouseWheel: true, click: true });
	    });
        myScroll.scrollToElement(document.querySelector('.categroy-list li:nth-child(' + ($(this).parent().index()+1) + ')'), 1000);
	});

    $('#categroy-cnt').on('click','.brand', function(){
        $('.pre-loading').show();
        get_brand_recommend();
    });
});

//默认获取第一个推荐列表
function get_brand_recommend() {
    $('.category-item').removeClass('selected');
    $('.brand').parent().addClass('selected');
    $.getJSON(ApiUrl + '/index.php?act=brand&op=recommend_list', function(result){
        var data = result.datas;
        data.WapSiteUrl = WapSiteUrl;
        var html = template.render('brand-one', data);
        $("#categroy-rgt").html(html);
        $('.pre-loading').hide();
        new IScroll('#categroy-rgt', { mouseWheel: true, click: true });
    });
}

//初始化加载第一个
function initPage(gc_id){
        $.getJSON(ApiUrl + '/index.php?act=goods_class&op=get_child_all', {gc_id:gc_id}, function(result){
            var data = result.datas;
            data.WapSiteUrl = WapSiteUrl;
            var html = template.render('category-two', data);
            $("#categroy-rgt").html(html);
            new IScroll('#categroy-rgt', { mouseWheel: true, click: true });
        });
        // myScroll.scrollToElement(document.querySelector('.categroy-list li:nth-child(' + ($(this).parent().index()+1) + ')'), 1000);
}