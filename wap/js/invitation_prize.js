var key = getCookie('key');
var userId = getQueryString("userId");
var id = '';
var user_name='';
$(function(){
	//分享
	if(navigator.userAgent.indexOf("Html5Plus") > -1){
		$('.tops').hide();
		id = key;
	}else{
		$('#header').hide();
		id = userId;
	}
	$.ajax({
	 	type:'post',
        url:ApiUrl+"/index.php?act=member_index",
        data:{key:id},
        dataType:'json',
        //jsonp:'callback',
        success:function(result){
          checkLogin(result.login);
        	var avatar = result.datas.member_info.avatar;
        	user_name = result.datas.member_info.user_name;
          getzccode(result.datas.member_info.invite_code)
          $('#yqm').text(result.datas.member_info.invite_code);
        	$('.headimg').attr('src',avatar);
        	$('#name').text(user_name);
        }
	});
    document.getElementById("sharex").addEventListener("click", function() {
      if(key){
          if(navigator.userAgent.indexOf("Html5Plus") > -1) {
            // if(key){
              //5+ 原生分享
              window.plusShare({
                  title: user_name+'邀你开启天香之旅',//应用名字
                  content: "香生活一网打尽",
                  href: WapSiteUrl+'/tmpl/member/invitation_prize.html?userId='+key+'',//分享出去后，点击跳转地址
                  thumbs: ["https://shop.tianxiangmall.net/wap/images/thumbs.png"] //分享缩略图
              }, function(result) {
                  //分享回调
              });
            // }else{
            //     window.location.href=WapSiteUrl+'/tmpl/member/login.html';
            // }
          } else {
              alert('请在app打开实现分享');
              //原有wap分享实现 
          }
        }else{
          window.location.href=WapSiteUrl+'/tmpl/member/login.html';
        }
    });
})

function toxq(id){
	if(navigator.userAgent.indexOf('Html5Plus') == -1){
        getxqcode($('#yqm').text(),id)
        window.location=WapSiteUrl+"/tmpl/download.html";
        $('.xfade,.frame').show();
	}else{
		window.location.href=WapSiteUrl+'/tmpl/product_detail.html?goods_id='+id+'';
	}
}

function hide(){
  $('.xfade,.frame').hide();
}
//获取注册页面的二维码
function getzccode(code){
     $.ajax({
        type:'post',
        url:ApiUrl+"/index.php?act=wxlogin&op=getwxacodeunlimit",
        data:{scene:'invite='+code+'',page:'pages/register/index'},
        dataType:'json',
        //jsonp:'callback',
        success:function(result){
            $('#chart').attr('src','../../../data/upload/mobile/wxacode/'+result.datas);
           // get(result.access_token);
        },
        fail:function(result){
            console.log(result);
        }
    });
}

//获取跳转到商品详情的二维码
function getxqcode(code,id){
    $.ajax({
        type:'post',
        url:ApiUrl+"/index.php?act=wxlogin&op=getwxacodeunlimit",
        data:{scene:'id='+id+'&invite='+code+'',page:'pages/goods/detail/index'},
        dataType:'json',
        //jsonp:'callback',
        success:function(result){
           $('#x_code').attr('src','../../../data/upload/mobile/wxacode/'+result.datas);
        },
        fail:function(result){
            console.log(result);
        }
    });
}