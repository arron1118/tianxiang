<?php

defined('In33hao') or exit('Access Invalid!');
class data_dayModel extends Model{
    
     public function __construct(){
        parent::__construct('data_day');
    }
    
    //每日分成份额
    public function data_return(){
      
    
        $cash_list=Model('member');
        $log_list=Model('coupon_log');
       
       $list=$cash_list->select();
       
  
       $cash_log=[];
       $member=[];
       foreach($list as $k=>$v){
           $member=[
               'member_points' =>array('exp','member_points-'.$v['member_points']*0.001),
               'available_rc_balance'=>['exp','available_rc_balance+'.$v['member_points']*0.001]
           ];
           
            $refers=$cash_list->where(['member_id'=>$v['member_id']])->update($member);
         if($refers){
              $cash_log['member_id'] = $v['member_id'];
            $cash_log['time'] = time();
            $cash_log['type'] = 1;
            $cash_log['quota']=$v['member_points']*0.001;
            $log_list->insert($cash_log);
         }
       }
        
     
       exit();
    }
}

