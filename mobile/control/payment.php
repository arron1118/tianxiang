<?php

/**
 * 支付回调
 *

 */
defined('In33hao') or exit('Access Invalid!');

class paymentControl extends mobileHomeControl {

    private $payment_code;

    public function __construct() {
        parent::__construct();

        $this->payment_code = $_GET['payment_code'];
    }

    /**
     * 支付回调
     */
    public function returnOp() {
        unset($_GET['act']);
        unset($_GET['op']);
        unset($_GET['payment_code']);

        $payment_api = $this->_get_payment_api();

        $payment_config = $this->_get_payment_config();

        $callback_info = $payment_api->getReturnInfo($payment_config);

        if ($callback_info) {
            //验证成功
            $result = $this->_update_order($callback_info['out_trade_no'], $callback_info['trade_no']);
            if ($result['state']) {
                Tpl::output('result', 'success');
                Tpl::output('message', '支付成功');
            } else {
                Tpl::output('result', 'fail');
                Tpl::output('message', '支付失败');
            }
        } else {
            //验证失败
            Tpl::output('result', 'fail');
            Tpl::output('message', '支付失败');
        }

        Tpl::showpage('payment_message');
    }

    /**
     * 支付提醒
     */
    public function notifyOp() {
        $point_log=Model('coupon_log');
        if ($this->payment_code == 'alipay') {
            $pay_sn = $_POST['out_trade_no'];
            $total = $_POST['total_fee'];

            $order = Model('orders');
            $find = $order->where(['pay_sn' => $pay_sn])->find();
            if ($find['pay_from'] != 1) {
                $amount = $find['order_amount'];

                $coupons=$find['order_coupons'];

                $state = $order->where(['pay_sn' => $pay_sn])->update(['order_state' => 20, 'order_coupons' => $coupons, 'pay_from' => 1]);
                $member_id = $find['buyer_id'];
                      if($find['order_cash']){
                       
                        $insert1 = [
                            'type' =>0,
                            'order_id' =>$find['order_id'],
                            'member_id' => $find['buyer_id'],
                            'time' => time(),
                            'cash' =>$find['order_cash']
                        ];
                        
                        Model('cash_log')->insert($insert1);
                    }
                    
                    if($find['order_coupons']){
                           $insert2 = [
                            'type' => 4,
                            'order_id' =>$find['order_id'],
                            'member_id' => $find['buyer_id'],
                            'time' => time(),
                            'quota' => $find['order_coupons']
                        ];
                        
                        Model('coupon_log')->insert($insert2);
                    }
                try {
                      
                        // 会员验证
                        $member = Model('member');
                        $member_info = $member->getMemberInfo(['member_id' =>$member_id]);

                        $oid = $order_list[0]['order_id'];

                        $order_goods = Model('order_goods')->where(['order_id' => $oid])->select();
                        foreach ($order_goods as $k => $v) {
                            $goods = Model('goods')->where(['goods_id' => $v['goods_id']])->find();
                            $vtype[$k] = $goods['svip_gift'];
                            
                        }
                        if (in_array(1, $vtype)) {
                            $return = $member->svip($member_info['member_id'], $order_list[0]);
                        } elseif (in_array(2, $vtype)) {
                            $return = $member->superb($member_info['member_id'], $order_list[0]);
                        }

                         $order_invite=$find;
                        $invite = Model('invite');

                        $invite_info = $invite->where(['member_id' => $member_info['member_id']])->find();

                        if ($invite_info) {
                              $amount=$order_invite['order_amount'];
                            $invite_id = $invite_info['invite_id'];
                            $seller_id = $invite_info['seller_id'];

                            if ($invite_id) {
                                $one_lv = $member->getMemberInfo($invite_id);
                                if ($one_lv['svip'] == 1 && $one_lv['seller'] != 1) {
                                  $cash=round($amount*0.05,2);
                                  $re=$member->where(['member_id'=>$one_lv['member_id']])->setInc('member_cash',$cash);
                                  if($re){
                                      $cash_log=[
                                          'member_id' => $one_lv['member_id'],
                                            'cash' => $cash,
                                            'time' => time(),
                                            'type' => 3,
                                            'order_id' =>$order_list['order_id']
                                        ];
                                  }
                                }
                            }
                            

                            //分校分成END
                        }
                        $this->commit();
                        return true;
                    } catch (Exception $e) {
                        $this->rollback();
                        return false;
                    }

             
            } else {
                echo "success";
                exit();
            }
        }

        // wxpay_jsapi
        if ($this->payment_code == 'wxpay') {



            $xmlData = file_get_contents('php://input');

            libxml_disable_entity_loader(true);
            $data = json_decode(json_encode(simplexml_load_string($xmlData, 'SimpleXMLElement', LIBXML_NOCDATA)), true);

            $pay_sn = $data['out_trade_no'];
            $order = Model('orders');
            $find = $order->where(['pay_sn' => $pay_sn])->find();
            $member_id = $find['buyer_id'];
            if ($find['pay_from'] != 2) {
                $total = round($data['total_fee'] * 0.01, 2);
                $state = $order->where(['pay_sn' => $pay_sn])->update(['order_state' => 20,'pay_from' => 2]);
                if($find['order_cash']){
                       
                        $insert1 = [
                            'type' =>0,
                            'order_id' =>$find['order_id'],
                            'member_id' => $find['buyer_id'],
                            'time' => time(),
                            'cash' =>$find['order_cash']
                        ];
                        
                        Model('cash_log')->insert($insert1);
                    }
                    
                    if($find['order_coupons']){
                           $insert2 = [
                            'type' => 4,
                            'order_id' =>$find['order_id'],
                            'member_id' => $find['buyer_id'],
                            'time' => time(),
                            'quota' => $find['order_coupons']
                        ];
                        
                        Model('coupon_log')->insert($insert2);
                    }
                try {
                      
                        // 会员验证
                        $member = Model('member');
                        $member_info = $member->getMemberInfo(['member_id' =>$member_id]);

                        $oid = $order_list[0]['order_id'];

                        $order_goods = Model('order_goods')->where(['order_id' => $oid])->select();
                        foreach ($order_goods as $k => $v) {
                            $goods = Model('goods')->where(['goods_id' => $v['goods_id']])->find();
                            $vtype[$k] = $goods['svip_gift'];
                            
                        }
                        if (in_array(1, $vtype)) {
                            $return = $member->svip($member_info['member_id'], $order_list[0]);
                        } elseif (in_array(2, $vtype)) {
                            $return = $member->superb($member_info['member_id'], $order_list[0]);
                        }

                         $order_invite=$find;
                        $invite = Model('invite');

                        $invite_info = $invite->where(['member_id' => $member_info['member_id']])->find();

                        if ($invite_info) {
                              $amount=$order_invite['order_amount'];
                            $invite_id = $invite_info['invite_id'];
                            $seller_id = $invite_info['seller_id'];

                            if ($invite_id) {
                                $one_lv = $member->getMemberInfo($invite_id);
                                if ($one_lv['svip'] == 1 && $one_lv['seller'] != 1) {
                                  $cash=round($amount*0.05,2);
                                  $re=$member->where(['member_id'=>$one_lv['member_id']])->setInc('member_cash',$cash);
                                  if($re){
                                      $cash_log=[
                                          'member_id' => $one_lv['member_id'],
                                            'cash' => $cash,
                                            'time' => time(),
                                            'type' => 3,
                                            'order_id' =>$order_list['order_id']
                                        ];
                                  }
                                }
                            }
                            

                            //分校分成END
                        }
                        $this->commit();
                        return true;
                    } catch (Exception $e) {
                        $this->rollback();
                        return false;
                    }
                
                
           
            } else {


                return '<xml><return_code><![CDATA[SUCCESS]]></return_code><return_msg><![CDATA[OK]]></return_msg></xml>';
            }

            exit;
        }

        // 恢复框架编码的post值
        $_POST['notify_data'] = html_entity_decode($_POST['notify_data']);

        $payment_api = $this->_get_payment_api();

        $payment_config = $this->_get_payment_config();

        $callback_info = $payment_api->getNotifyInfo($payment_config);

        if ($callback_info) {
            //验证成功
            $result = $this->_update_order($callback_info['out_trade_no'], $callback_info['trade_no']);
            if ($result['state']) {
                echo 'success';
                die;
            }
        }

        //验证失败
        echo "fail";
        die;
    }

    public function notify1Op() {

        if ($this->payment_code == 'alipay') {
            $pay_sn = $_POST['out_trade_no'];
            $order = Model('recharge_log');


            $member = Model('member');
            $money = $order->where(['pay_sn' => $pay_sn, 'type' => 0])->find();
            if ($money) {
                $state = $order->where(['pay_sn' => $pay_sn])->update(['type' => 1, 'pay_from' => 1]);
                if ($state) {
                    $member->where(['member_id' => $money['member_id']])->setInc('available_rc_balance', $money['money']);
                }
            } else {
                echo 'success';
                exit();
            }
        }

        // wxpay_jsapi
        if ($this->payment_code == 'wxpay') {
            $xmlData = file_get_contents('php://input');

            libxml_disable_entity_loader(true);
            $data = json_decode(json_encode(simplexml_load_string($xmlData, 'SimpleXMLElement', LIBXML_NOCDATA)), true);

            $pay_sn = $data['out_trade_no'];
            $order = Model('recharge_log');

            $state = $order->where(['pay_sn' => $pay_sn])->update(['type' => 1, 'pay_from' => 2]);
            if ($state) {
                $member = Model('member');
                $money = $order->where(['pay_sn' => $pay_sn])->find();
                $ok = $member->where(['member_id' => $money['member_id']])->setInc('available_rc_balance', $money['money']);
                if ($ok) {
                    echo "<xml><return_code><![CDATA[SUCCESS]]></return_code><return_msg><![CDATA[OK]]></return_msg></xml>";
                    exit();
                }
            }
        }
    }

    /**
     * 支付宝移动支付
     */
    public function notify_alipay_nativeOp() {
        $this->payment_code = 'alipay_native';
        $inc_file = BASE_PATH . DS . 'api' . DS . 'payment' . DS . $this->payment_code . DS . $this->payment_code . '.php';

        if (is_file($inc_file)) {
            require($inc_file);
        }

        $payment_config = $this->_get_payment_config();
        $payment_api = new $this->payment_code();
        $payment_api->payment_config = $payment_config;
        $payment_api->alipay_config['partner'] = $payment_config['alipay_partner'];

        if ($payment_api->verify_notify()) {

            //商户订单号
            $out_trade_no = $_POST['out_trade_no'];
            //支付宝交易号
            $trade_no = $_POST['trade_no'];

            //交易状态
            $trade_status = $_POST['trade_status'];

            if ($_POST['trade_status'] == 'TRADE_FINISHED' || $_POST['trade_status'] == 'TRADE_SUCCESS') {
                $result = $this->_update_order($out_trade_no, $trade_no);
                if (!$result['state']) {
                    logResult("订单状态更新失败" . $out_trade_no);
                }
            }
            exit("success");
        } else {
            logResult("verifyNotify验证失败" . $out_trade_no);
            exit("fail");
        }
    }

    /**
     * 获取支付接口实例
     */
    private function _get_payment_api() {
        $inc_file = BASE_PATH . DS . 'api' . DS . 'payment' . DS . $this->payment_code . DS . $this->payment_code . '.php';

        if (is_file($inc_file)) {
            require($inc_file);
        }

        $payment_api = new $this->payment_code();

        return $payment_api;
    }

    /**
     * 获取支付接口信息
     */
    private function _get_payment_config() {
        $model_mb_payment = Model('mb_payment');

        //读取接口配置信息
        $condition = array();
        if ($this->payment_code == 'wxpay3') {
            $condition['payment_code'] = 'wxpay';
        } else {
            $condition['payment_code'] = $this->payment_code;
        }
        $payment_info = $model_mb_payment->getMbPaymentOpenInfo($condition);

        return $payment_info['payment_config'];
    }

    /**
     * 更新订单状态
     */
    private function _update_order($out_trade_no, $trade_no) {
        $model_order = Model('order');
        $logic_payment = Logic('payment');

        $tmp = explode('_', $out_trade_no);
        $out_trade_no = $tmp[0];
        if (!empty($tmp[1])) {
            $order_type = $tmp[1];
        } else {
            $order_pay_info = Model('order')->getOrderPayInfo(array('pay_sn' => $out_trade_no));
            if (empty($order_pay_info)) {
                $order_type = 'v';
            } else {
                $order_type = 'r';
            }
        }

        // wxpay_jsapi
        $paymentCode = $this->payment_code;
        if ($paymentCode == 'wxpay_jsapi') {
            $paymentCode = 'wx_jsapi';
        } elseif ($paymentCode == 'wxpay3') {
            $paymentCode = 'wxpay';
        } elseif ($paymentCode == 'alipay_native') {
            $paymentCode = 'ali_native';
        }

        if ($order_type == 'r') {
            $result = $logic_payment->getRealOrderInfo($out_trade_no);
            if (intval($result['data']['api_pay_state'])) {
                return array('state' => true);
            }
            $order_list = $result['data']['order_list'];
            $result = $logic_payment->updateRealOrder($out_trade_no, $paymentCode, $order_list, $trade_no);

            $api_pay_amount = 0;
            if (!empty($order_list)) {
                foreach ($order_list as $order_info) {
                    $api_pay_amount += $order_info['order_amount'] - $order_info['pd_amount'] - $order_info['rcb_amount'];
                }
            }
            $log_buyer_id = $order_list[0]['buyer_id'];
            $log_buyer_name = $order_list[0]['buyer_name'];
            $log_desc = '实物订单使用' . orderPaymentName($paymentCode) . '成功支付，支付单号：' . $out_trade_no;
        } elseif ($order_type == 'v') {
            $result = $logic_payment->getVrOrderInfo($out_trade_no);
            $order_info = $result['data'];
            if (!in_array($result['data']['order_state'], array(ORDER_STATE_NEW, ORDER_STATE_CANCEL))) {
                return array('state' => true);
            }
            $result = $logic_payment->updateVrOrder($out_trade_no, $paymentCode, $result['data'], $trade_no);

            $api_pay_amount = $order_info['order_amount'] - $order_info['pd_amount'] - $order_info['rcb_amount'];
            $log_buyer_id = $order_info['buyer_id'];
            $log_buyer_name = $order_info['buyer_name'];
            $log_desc = '虚拟订单使用' . orderPaymentName($paymentCode) . '成功支付，支付单号：' . $out_trade_no;
        }
        if ($result['state']) {
            //记录消费日志
            QueueClient::push('addConsume', array('member_id' => $log_buyer_id, 'member_name' => $log_buyer_name,
                'consume_amount' => ncPriceFormat($api_pay_amount), 'consume_time' => TIMESTAMP, 'consume_remark' => $log_desc));
        }

        return $result;
    }

}
