<?php

defined('In33hao') or exit('Access Invalid!');

class updateapkControl extends mobileHomeControl
{
    public function __construct()
    {
        parent::__construct();
    }
    
    /*一、数据结构
  appid
  ostype    操作系统类型  如 0/Android    1/iOS   
  version   最新版本  如1.2    2.0
  type        升级类型  0/可选升级   1制升级  
  url           下载url
  note         升级提示信息
  status     状态  如0有效 1无效
 cdate
二、接口
   check/update   检测是否有新版本   Get
   输入 
         appid,version
   输出
        status  0无最新版本   1发现新版本
        version
        note
        type
        url
先校验appid ，通过请求的useragent判断ostype，再判断是否有更大的版本存在，有则返回最大的版本号和下载地址，否者status=0*/
    
    public function update_apiOp(){
        //appid,version
       
   
        if($_GET['version']=='1.2'){
            $status=0;
        }else{
            $status=1;
            
        }

        if($_GET['os']=='iOS'){

            $ostype=1;
             $status=0;
        }else{
            $ostype=0;
        }
        output_data(
                [
                    'ostype'=> $ostype,
                    'status' =>$status, //0无最新版本   1发现新版本
                    'version' => '1.2',
                   'note' => '是否确定升级全新版本',
                    'type' => 0,
                    'url' => 'https://tianxiangmall.oss-cn-shenzhen.aliyuncs.com/tianxiangmall.v12.apk'
                ]
        );
    }
}


