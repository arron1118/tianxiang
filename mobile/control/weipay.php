<?php
/**
 * Created by PhpStorm.
 * User: arron
 * Date: 2018/6/12
 * Time: 10:59
 */

defined('In33hao') or exit('Access Invalid!');

/**
 * Class weipay
 * 微信小程序支付类
 */
class weipayControl extends mobileMemberControl {
    //=======【基本信息设置】=====================================
    //微信小程序身份的唯一标识
    protected $APPID = 'wx4e03075902530aba';//小程序appid
    protected $APPSECRET = '2c5b8bf6c331d487ec37559bd22a8d05';  //小程序密钥
    //受理商ID，身份标识
    protected $MCHID = '1502580271';//商户id
    //商户支付密钥Key
    protected $KEY = '9pJYqHSGvQH8IHEnltcbAgcKFlgtUIWi';
    //回调通知接口
    protected $APPURL =   'http://shop.tianxiangmall.net/mobile/api/payment/wxpayv3/notify_url.php';
    //交易类型
    protected $TRADETYPE = 'JSAPI';
    //商品类型信息
    protected $BODY = 'wx/book';

    /**
     * 微信支付类的构造函数
     * weipayControl constructor.
     */
    public function __construct(){
        parent::__construct();
    }

    /**
     * 微信支付类向外暴露的支付接口
     * @return array
     */
    public function payOp(){
        $result = $this->weixinapp();
        output_data($result);
    }

    /**
     * 对微信统一下单接口返回的支付相关数据进行处理
     * @return array
     */
    private function weixinapp(){
        $unifiedorder = $this->unifiedorder();
        $parameters = array(
            'appId' => $this->APPID, //小程序ID
            'timeStamp' => ''. time() . '', //时间戳
            'nonceStr' => $this->createNoncestr(),   //随机串
            'package' => 'prepay_id=' . $unifiedorder['prepay_id'], //数据包
            'signType' => 'MD5'   //签名方式
        );
        $parameters['paySign'] = $this->getSign($parameters);
        return $parameters;
    }

    /*
     * 请求微信统一下单接口
     */
    private function unifiedorder(){
        $openid = $_POST['openid'];
        $parameters = array(
            'appid' => $this->APPID,    //小程序id
            'mch_id' => $this->MCHID,    //商户id
            'spbill_create_ip' => $_SERVER['REMOTE_ADDR'],   //终端ip
            'notify_url' => $this->APPURL,   //通知地址
            'nonce_str' => $this->createNoncestr(),  //随机字符串
            'out_trade_no' => $_POST['out_trade_no'],  //商户订单编号
            'total_fee' => $_POST['total_fee'], //总金额
            'openid' => $openid,   //用户openid
            'trade_type' => $this->TRADETYPE, //交易类型
            'body' => $_POST['body'], //商品信息
        );
        $parameters['sign'] = $this->getSign($parameters);
        $xmlData = $this->arrayToXml($parameters);
        $xml_result = $this->postXmlCurl($xmlData,'https://api.mch.weixin.qq.com/pay/unifiedorder',60);
        $result = $this->xmlToArray($xml_result);
        return $result;
    }

    /**
     * 数组转字符串方法
     * @param $arr
     * @return string
     */
    protected function arrayToXml($arr){
        $xml = "<xml>";
        foreach ($arr as $key => $val)
        {
            if (is_numeric($val)){
                $xml .= "<" . $key . ">" . $val . "</" . $key . ">";
            }else{
                $xml .= "<" . $key . "><![CDATA[" . $val . "]]></" . $key . ">";
            }
        }
        $xml .= "</xml>";
        return $xml;
    }

    /**
     * @param $xml
     * @return mixed
     */
    protected function xmlToArray($xml){
        $array_data = json_decode(json_encode(simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
        return $array_data;
    }

    /**
     * 发送xml请求方法
     * @param $xml
     * @param $url
     * @param int $second
     * @return mixed
     * @throws WxPayException
     */
    private static function postXmlCurl($xml, $url, $second = 30)
    {
        $ch = curl_init();
        //设置超时
        curl_setopt($ch, CURLOPT_TIMEOUT, $second);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE); //严格校验
        //设置header
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        //要求结果为字符串且输出到屏幕上
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        //post提交方式
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
        curl_setopt($ch, CURLOPT_TIMEOUT, 40);
        set_time_limit(0);
        //运行curl
        $data = curl_exec($ch);
        //返回结果
        if ($data) {
            curl_close($ch);
            return $data;
        } else {
            $error = curl_errno($ch);
            curl_close($ch);
            throw new WxPayException("curl出错，错误码:$error");
        }
    }

    /**
     * 对要发送到微信统一下单接口的数据进行签名
     * @param $Obj
     * @return string
     */
    protected function getSign($Obj){
        foreach ($Obj as $k => $v){
            $Parameters[$k] = $v;
        }
        //签名步骤一：按字典序排序参数
        ksort($Parameters);
        $String = $this->formatBizQueryParaMap($Parameters, false);
        //签名步骤二：在string后加入KEY
        $String = $String . "&key=" . $this->KEY;
        //签名步骤三：MD5加密
        $String = md5($String);
        //签名步骤四：所有字符转为大写
        $result_ = strtoupper($String);
        return $result_;
    }

    /**
     * 排序并格式化参数方法，签名时需要使用
     * @param $paraMap
     * @param $urlencode
     * @return bool|string
     */
    protected function formatBizQueryParaMap($paraMap, $urlencode)
    {
        $buff = "";
        ksort($paraMap);
        foreach ($paraMap as $k => $v)
        {
            if($urlencode)
            {
                $v = urlencode($v);
            }
            //$buff .= strtolower($k) . "=" . $v . "&";
            $buff .= $k . "=" . $v . "&";
        }
        $reqPar = '';
        if (strlen($buff) > 0)
        {
            $reqPar = substr($buff, 0, strlen($buff)-1);
        }
        return $reqPar;
    }

    /**
     * 生成随机字符串方法
     * @param int $length
     * @return string
     */
    protected function createNoncestr($length = 32 ){
        $chars = "abcdefghijklmnopqrstuvwxyz0123456789";
        $str = "";
        for ( $i = 0; $i < $length; $i++ ) {
            $str .= substr($chars, mt_rand(0, strlen($chars)-1), 1);
        }
        return $str;
    }

    /**
     * 微信小程序获取用户openId
     */
    public function takeOidOp() {
        $url = "https://api.weixin.qq.com/sns/jscode2session?appid=" . $this->APPID . "&secret=" . $this->APPSECRET . "&js_code=" . $_POST['code'] . "&grant_type=authorization_code";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);  //终止从服务端验证
        // curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  //不直接输出
        curl_exec($curl);
        curl_close($curl);
        /*$result = json_decode($result, true);
        return $result['openid'];*/
    }

}