<?php
/**
 * 我的商城
 *

 */



defined('In33hao') or exit('Access Invalid!');

class member_indexControl extends mobileMemberControl {
    

    public function __construct(){
        
        parent::__construct();
    }

    /**
     * 我的商城
     */
    public function indexOp() {
         
        
        
        $member_id=$this->member_info['member_id'];
        
        $member_info = array();
        $member_info['user_name'] = $this->member_info['member_name'];
       
     
         
        $member_gradeinfo = Model('member')->getOneMemberGrade(intval($this->member_info['member_exppoints']));
        $member_info['level_name'] = $member_gradeinfo['level_name'];
        $member_info['favorites_store'] = Model('favorites')->getStoreFavoritesCountByMemberId($this->member_info['member_id']);
        $member_info['favorites_goods'] = Model('favorites')->getGoodsFavoritesCountByMemberId($this->member_info['member_id']);
        // 交易提醒
        $model_order = Model('order');
        $member_info['order_nopay_count'] = $model_order->getOrderCountByID('buyer', $this->member_info['member_id'], 'NewCount');
        $member_info['order_noreceipt_count'] = $model_order->getOrderCountByID('buyer', $this->member_info['member_id'], 'SendCount');
        $member_info['order_notakes_count'] = $model_order->getOrderCountByID('buyer', $this->member_info['member_id'], 'TakesCount');
        $member_info['order_noeval_count'] = $model_order->getOrderCountByID('buyer', $this->member_info['member_id'], 'EvalCount');
        
        // 售前退款
        $condition = array();
        $condition['buyer_id'] = $this->member_info['member_id'];
        $condition['refund_state'] = array('lt', 3);
        $member_info['return'] = Model('refund_return')->getRefundReturnCount($condition);
        
       
           
        $seller=Model('store')->where(['member_id'=>$member_id])->find();
            $buyer_info=Model('member')->where(['member_id'=>$member_id])->find();
           
       
            if($buyer_info['member_avatar']){
                 $member_info['avatar'] ='http://shop.tianxiangmall.net'.$buyer_info['member_avatar'];
            }else{
                 $member_info['avatar'] = getMemberAvatarForID($this->member_info['member_id']);
            }
           
           
          //余额 优惠券
          $member_info['money']=$buyer_info['available_rc_balance'];
          $member_info['cash_flow']=$buyer_info['member_points'];
            $member_info['invite_code']=$buyer_info['invite_code'];
            $member_info['member_cash']=$buyer_info['member_cash'];
            $member_info['user_name']=$buyer_info['member_name'];
        if(!$seller){
            
            $member_info['is_shop']=0;
        }else{
            $member_info['is_shop']=1;
            
        }
        if($buyer_info['seller']==0 && $buyer_info['svip']==0){
            $member_info['u_level']=0;
        }elseif($buyer_info['svip']==1){
            $member_info['u_level']=1;
        }elseif($buyer_info['seller']==1){
            $member_info['u_level']=2;
        }
        
        output_data(array('member_info' => $member_info));
    }
    
    /**
     * 我的资产
     */
    public function my_assetOp() {
        $param = $_GET;
        $fields_arr = array('point','predepoit','available_rc_balance','redpacket','voucher');
        $fields_str = trim($param['fields']);
        if ($fields_str) {
            $fields_arr = explode(',',$fields_str);
        }
        $member_info = array();
        if (in_array('point',$fields_arr)) {
            $member_info['point'] = $this->member_info['member_points'];
        }
        if (in_array('predepoit',$fields_arr)) {
            $member_info['predepoit'] = $this->member_info['available_predeposit'];
        }
        if (in_array('available_rc_balance',$fields_arr)) {
            $member_info['available_rc_balance'] = $this->member_info['available_rc_balance'];
        }
        if (in_array('redpacket',$fields_arr)) {
            $member_info['redpacket'] = Model('redpacket')->getCurrentAvailableRedpacketCount($this->member_info['member_id']);
        }
        if (in_array('voucher',$fields_arr)) {
            $member_info['voucher'] = Model('voucher')->getCurrentAvailableVoucherCount($this->member_info['member_id']);
        }
        output_data($member_info);
    }
    
    

    
    public function invite_detailOp(){
      
     $member_info = array();
        $member_info['user_name'] = $this->member_info['member_name'];
        $member_info['avatar'] = getMemberAvatarForID($this->member_info['member_id']);
        
          $member_info['invite_image']='../../images/code/'.$this->member_info['invite_code'].'.png';
       
           
        $member_gradeinfo = Model('member')->getOneMemberGrade(intval($this->member_info['member_exppoints']));
       
        // 交易提醒
       
        
        // 售前退款
        $condition = array();
        $condition['buyer_id'] = $this->member_info['member_id'];
        $condition['refund_state'] = array('lt', 3);
        $member_info['return'] = Model('refund_return')->getRefundReturnCount($condition);
        $member_info['invite_code']=$this->member_info['invite_code'];
        $invite=Model('invite')->where(['invite_id'=>$this->member_info['member_id']])->select();
       
        foreach($invite as $k=>$v){
           $uname=Model('member')->where(['member_id'=>$v['member_id']])->field('member_name,member_mobile')->find();
           $new[$k]['uname']=$uname['member_name'];
           $new[$k]['member_mobile']=$uname['member_mobile'];
           $new[$k]['ctime']=$v['rtime'];
           
        }
        
        $member_info['invite_list']=$new;
        
        output_data(array('member_info' => $member_info));
    }
    
    public function get_redOp(){
            $uid=$this->member_info['member_id'];
            $red=Model('coupon_list')->where(['member_id'=>$uid])->order('coupon_id desc')->page(10)->select();
       
              $page_count = Model('member')->gettotalpage();
   
             
            output_data(array('data' => $red), mobile_page($page_count));
            
            
    }
    
    public function get_red_idOp(){
        
        $coupon_id=$_POST['id'];
        $uid=$this->member_info['member_id'];
        
        $get=Model('coupon_list')->where(['coupon_id'=>$coupon_id,'type'=>0])->update(['type'=>1]);
        
        if($get){
            $memeber=Model('member')->where(['member_id'=>$uid])->setInc('available_rc_balance',$_POST['money']);
            if($member){
                
                $log_pay=Model('recharge_log');
                        $insert=[
                            'type'=>1,
                            'pay_from'=>0,
                            'pay_sn'=>time().$member['id'],
                            'member_id'=>$member['id'],
                            'ctime'=>time(),
                            'money'=>$_POST['money']
                        ];
                        
                        $pay_red=$log_pay->where(['pay_sn'=>$pay_sn])->insert($insert);
                        if($pay_red){
             output_data($memeber); 
                        }
             
            }
             }
        
       
        
    }
    
    //余额列表
    public function cash_listOp(){
        $uid=$this->member_info['member_id'];
        $log=Model('recharge_log');
        $list=$log->where(['member_id'=>$uid,'type'=>1])->order('id desc')->page(10)->select();
        
            $page_count = Model('member')->gettotalpage();
      
             
            output_data(array('data' => $list), mobile_page($page_count));
        
        
  
        
    }


//香券列表
  public function coupons_listOp(){
        $uid=$this->member_info['member_id'];
        $log=Model('coupon_log');
        $list=$log->where(['member_id'=>$uid])->order('id desc')->page(10)->select();
        
            $page_count = Model('member')->gettotalpage();
      
             
            output_data(array('data' => $list), mobile_page($page_count));
        
        
  
        
    }
    
    
        //余额列表
    public function member_cash_listOp(){
        $uid=$this->member_info['member_id'];
        $log=Model('cash_log');
        $list=$log->where(['member_id'=>$uid])->order('id desc')->page(10)->select();
        
            $page_count = Model('member')->gettotalpage();
      
             
            output_data(array('data' => $list), mobile_page($page_count));

    }
    
   //修改用户名
    public function member_nameOp(){
        $uid=$this->member_info['member_id'];
        
        $name=$_POST['username'];
        
        if($name){
            $data=[
                'member_name'=>$name
            ];
            $res=Model('member')->where(['member_id'=>$uid])->update($data);
            if($res){
                $retrun=[
                    'username'=>$name
                ];
                  output_data(array('data' => $retrun));
            }
        }
    }
    
    //是否又未支付订单
    public function ispayOp(){
        $uid=$this->member_info['member_id'];
        
      
       $order=Model('orders')->where(['buyer_id'=>$uid,'order_state'=>10])->find();
       
       if($order){
           $data['type']=1;
       }else{
           $data['type']=0;
       }
       
       output_data(array('data'=>$data));
    } 
    
    

    

 
}





    
    