<?php
/**
 * Created by PhpStorm.
 * User: arron
 * Date: 2018/6/19
 * Time: 17:31
 *
 * 微信小程序微信用户登录与注册
 */

defined('In33hao') or exit('Access Invalid!');

class wxloginControl extends mobileHomeControl {
    //=======【基本信息设置】=====================================
    //微信小程序身份的唯一标识
    protected $APPID = 'wx4e03075902530aba';//小程序appid
    protected $APPSECRET = '2c5b8bf6c331d487ec37559bd22a8d05';  //小程序密钥
    //受理商ID，身份标识
    protected $MCHID = '1502580271';//商户id
    //商户支付密钥Key
    protected $KEY = '9pJYqHSGvQH8IHEnltcbAgcKFlgtUIWi';
    //回调通知接口
    protected $APPURL =   'http://shop.tianxiangmall.net/mobile/api/payment/wxpayv3/notify_url.php';
    //交易类型
    protected $TRADETYPE = 'JSAPI';
    //商品类型信息
    protected $BODY = 'wx/book';

    /**
     * 微信支付类的构造函数
     * weipayControl constructor.
     */
    public function __construct(){
        parent::__construct();
    }

    /**
     * 微信用户登录
     */
    public function loginOp()
    {
        $user_info = $_POST;
        if (!$user_info['unionid']) {
            $user_info['unionid'] = $user_info['openid'];
        }
        $user_info['weixin_info'] = serialize(['unionid' => $user_info['unionid'], 'nickname' => $user_info['nickName'], 'openid' => $user_info['openid']]);
        if(!empty($user_info['unionid'])) {
            $unionid = $user_info['unionid'];
            $model_member = Model('member');
            $member = $model_member->getMemberInfo(array('weixin_unionid'=> $user_info['unionid']));
            if(!empty($member)) {//会员信息存在时自动登录
                $model_member->createSession($member);
                output_data($member);
            } else {//自动注册会员并登录
                //$this->register($user_info);
                exit;
            }
        }
    }

    /**
     * 获取微信用户个人信息
     * @param string $code
     * @return array
     */
    public function getWxUserInfo($code){
        $access_token = $this->getAccessToken();
        $userinfo = $this->takeOid();
        $user_info = array();
        if(!empty($access_token['access_token']) && !empty($userinfo['openid'])) {
            $token = $access_token['access_token'];
            $openid = $userinfo['openid'];
            $url = 'https://api.weixin.qq.com/cgi-bin/user/info?access_token=' . $token . '&openid=' . $openid . '&lang=zh_CN';
            $result = $this->getUrlContents($url);//获取用户个人信息
            var_dump($result);exit();
            $user_info = json_decode($result, true);
            if(empty($user_info['unionid'])) {
                $user_info['unionid'] = $user_info['openid'];
            }
            $weixin_info = array();
            $weixin_info['unionid'] = $user_info['unionid'];
            $weixin_info['nickname'] = $user_info['nickname'];
            $weixin_info['openid'] = $user_info['openid'];
            $user_info['weixin_info'] = serialize($weixin_info);
        }

        return $user_info;
    }

    /**
     * OAuth2.0授权认证
     * @param string $url
     * @return string
     */
    public function getUrlContents($url){
        return $this->curlExec($url);
        if (ini_get("allow_url_fopen") == "1") {
            return file_get_contents($url);
        } else {
            return $this->curlExec($url);
        }
    }

    /**
     * 登录生成token
     */
    private function _get_token($member_id, $member_name, $client) {
        $model_mb_user_token = Model('mb_user_token');

        //生成token
        $mb_user_token_info = array();
        $token = md5($member_name . strval(TIMESTAMP) . strval(rand(0,999999)));
        $mb_user_token_info['member_id'] = $member_id;
        $mb_user_token_info['member_name'] = $member_name;
        $mb_user_token_info['token'] = $token;
        $mb_user_token_info['login_time'] = TIMESTAMP;
        $mb_user_token_info['client_type'] = $client;

        /**
         * 更新用户 token 表
         */
        $result = $model_mb_user_token->addMbUserToken($mb_user_token_info);

        return $result ? $result : null;
    }

    /**
     * 微信小程序获取用户openId
     */
    public function takeOidOp() {
        $url = "https://api.weixin.qq.com/sns/jscode2session?appid=" . $this->APPID . "&secret=" . $this->APPSECRET . "&js_code=" . $_POST['code'] . "&grant_type=authorization_code";
        $res = $this->curlExec($url);
        echo json_encode($res);
    }

    /**
     * 获取微信 token_access
     */
    private function curlExec($url)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);  //终止从服务端验证
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  //不直接输出
        $result = curl_exec($curl);
        curl_close($curl);
        return json_decode($result, true);
    }

    private function getAccessToken()
    {
        $tmpData = $_SESSION['wxdata'];
        if (!$tmpData || $tmpData['expires_in'] < time()) {
            $url = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=' . $this->APPID . '&secret=' . $this->APPSECRET;
            $result = $this->curlExec($url);
            $result['expires_in'] = time() + $result['expires_in'];
            $_SESSION['wxdata'] = $tmpData = $result;
        }

        return $tmpData;
    }

    public function getwxacodeunlimitOp()
    {
        //$path = 'pages/register/index?invite=939162';
        $path = $_POST['page'];
        $scene = $_POST['scene'];
        $filename = md5($path . $scene) . '.jpg';
        $filepath = BASE_UPLOAD_PATH . '/mobile/wxacode/' . $filename;
        if (!file_exists($filepath)) {
            $url = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=' . $this->APPID . '&secret=' . $this->APPSECRET;
            $result = $this->curlExec($url);

            $url = 'https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token=' . $result['access_token'];
            $data = [
                'scene' => $scene,
                'page' => $path
            ];

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);  //终止从服务端验证
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
            curl_setopt($curl, CURLOPT_HTTPHEADER, ["Accept-Charset: utf-8"]);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  //不直接输出
            $result = curl_exec($curl);
            curl_close($curl);
            $ch = fopen($filepath, 'w');
            fwrite($ch, $result);
            fclose($ch);
        }

        output_data($filename);
    }

    /**
     * 登录
     */
    public function indexOp(){
        if(empty($_POST['username']) || empty($_POST['password']) || !in_array($_POST['client'], $this->client_type_array)) {
            output_error('登录失败');
        }

        $model_member = Model('member');

        $login_info = array();
        $login_info['user_name'] = $_POST['username'];
        $login_info['password'] = $_POST['password'];
        $member_info = $model_member->login($login_info);
        if(isset($member_info['error'])) {
            output_error($member_info['error']);
        } else {
            $token = $this->_get_token($member_info['member_id'], $member_info['member_name'], $_POST['client']);
            if($token) {
                output_data(array('username' => $member_info['member_name'], 'userid' => $member_info['member_id'], 'key' => $token));
            } else {
                output_error('登录失败');
            }
        }
    }

    /**
     * 注册
     */
    public function registerOp(){
        $model_member   = Model('member');
        $logic_connect_api = Logic('connect_api');
        $state_data = $logic_connect_api->checkSmsCaptcha($_POST['phone1'], $_POST['phy'], 1);
        if(!$state_data['state']){
            output_error('手机验证码错误');
        }
        $register_info = array();
//        $register_info['username'] = $_POST['username'];
        $register_info['password'] = $_POST['password'];

        $register_info['email'] = $_POST['email'];
        $register_info['invite'] = $_POST['invite'];
        $register_info['phone1']=$_POST['phone1'];

        $member_info = $model_member->register($register_info);
        if(!isset($member_info['error'])) {
            $token = $this->_get_token($member_info['member_id'], $member_info['member_name'], $_POST['client']);
            if($token) {
                output_data(array('username' => $member_info['member_name'], 'userid' => $member_info['member_id'], 'key' => $token));
            } else {
                output_error('注册失败');
            }
        } else {
            output_error($member_info['error']);
        }

    }
}
