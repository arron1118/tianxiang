<?php
/*充值*/



defined('In33hao') or exit('Access Invalid!');

class member_rechargeControl extends mobileMemberControl {
    
  
     public function __construct(){
        parent::__construct();
        
        if($_GET['type']=='zfb'){
            $payment_code='alipay';
        }elseif($_GET['type']=='wx'){
            $payment_code='wxpay';
        }  
   
         $condition = array();
            $model_mb_payment = Model('mb_payment');
            $condition['payment_code'] = $payment_code;
            $mb_payment_info = $model_mb_payment->getMbPaymentOpenInfo($condition);
              if(!$mb_payment_info) {
                output_error('支付方式未开启');
            }
           
            $this->payment_code = $payment_code;
            $this->payment_config = $mb_payment_info['payment_config'];
   
            
    }
    
    public function rechargeOp(){
       
        $member_id=$this->member_info['member_id'];
        $data=[];
        $data['member_id']=$member_id;
        $data['money']=$_GET['money'];
        $data['ctime']=time();
        $data['pay_sn']=date('Ymd') . str_pad(mt_rand(1, 99999), 5, '0', STR_PAD_LEFT);
       $recharge=Model('recharge_log');
       
       $res=$recharge->insert($data);
       if($res){
           $this->to_pay($data);
       }
    }
    
    public function to_pay($data){
       
         if(   $this->payment_code=='wxpay'){
            $this->_api_wechatpay($data);
        }
          if(   $this->payment_code=='alipay'){
            $this->_api_alibaba($data);
        }
        
    }
    
    public function _api_alibaba($data){
           $inc_file = BASE_PATH.DS.'api'.DS.'payment'.DS.$this->payment_code.DS.'index.php';
        
      
        require($inc_file);
         $order_list=[];
         $order_list['order_list'][0]['order_amount'] = $data['money'];
        $order_list['subject'] = '订单号' . $data['pay_sn'];
        $order_list['pay_sn'] = $data['pay_sn'];
       //MOBILE_SITE_URL.
      $alipay=new to_alipay();
      $notyfy_url=MOBILE_SITE_URL.'/api/payment/alipay/notify.php';
      
       $config=$this->payment_config;
      $alipay->to_pay($order_list,$config,$notyfy_url);
      exit();

            
    }
    
    
    
    
    public function _api_wechatpay($data){
       
        $inc_file = BASE_PATH . DS . 'api' . DS . 'payment' . DS . $this->payment_code . 'v3' . DS . 'index.php';
        require($inc_file);
         $order_list=[];
         $order_list['order_list'][0]['order_amount'] = $data['money'];
        $order_list['subject'] = '订单号' . $data['pay_sn'];
        $order_list['pay_sn'] = $data['pay_sn'];
        $notyfy_url = MOBILE_SITE_URL . '/api/payment/wxpayv3/notify.php';
        
         $wechatpay=new wechat_pay();
        $config = $this->payment_config;
        $wechatpay->to_wechat_pay($order_list, $config, $notyfy_url);
        exit();
    }
    


}