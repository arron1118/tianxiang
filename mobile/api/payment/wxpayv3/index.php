<?php
header('Access-Control-Allow-Origin: *');
header('Content-type: text/plain');

require_once "WxPay.Api.php";
require_once "WxPay.Data.php";

class wechat_pay{
// 获取支付金额
    
    public function to_wechat_pay($order_list, $config, $notify_url){
$amount='';
if($_SERVER['REQUEST_METHOD']=='POST'){
    $amount=$_POST['total'];
}else{
    $amount=$_GET['total'];
}
$amount=$order_list['order_list'][0]['order_amount'];

$total = floatval($amount);
$total = round($total*100); // 将元转成分
if(empty($total)){
    $total = 100;
}

// 商品名称
$subject = $order_list['subject'];
// 订单号，示例代码使用时间值作为唯一的订单ID号
$out_trade_no =$order_list['pay_sn'];

$unifiedOrder = new WxPayUnifiedOrder();
$unifiedOrder->SetBody($subject);//商品或支付单简要描述
$unifiedOrder->SetOut_trade_no($out_trade_no);
$unifiedOrder->SetTotal_fee($total);
$unifiedOrder->SetNotify_url($notify_url);
$unifiedOrder->SetTrade_type("APP");
$result = WxPayApi::unifiedOrder($unifiedOrder);
if (is_array($result)) {
    echo json_encode($result);
}
    }
}
?>