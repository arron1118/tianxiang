<?php
/**
 * 处理APP支付一步通知通知地址
 */
 //这是签名验证串，在 App配置 查询或重置
$secretKey = "q76wwaest07y506IJAkGwttNcVXdq8lw";
 
$kv = array();
foreach ($_POST as $key => $val) {
    $kv[$key] = $val;
}
ksort($kv);
reset($kv);
$param = '';
foreach ($kv AS $key => $val) {
    if ($key != 'sign') {
        $param .= "$key=$val&";
    }
}
$param = substr($param, 0, -1).$secretKey;
$verify_result = (md5($param) == strtolower($kv["sign"]));
if ($verify_result) {
    //获取有效的参数值
    $tradeid = $kv["tradeid"];  //支付平台上的交易号
    $orderid = $kv["orderid"];  //订单号
    $amount  = $kv["amount"] ;  //支付金额
    $channel = $kv["channel"];  //支付渠道, 0 微信，1 支付宝
 
    //用上面的参数处理支付成功的后续业务
    //请在这里自由发挥，尽量不要修改其他地方
    //do something...
 
    //处理成功后，http 要返回 OK 字符，否则你的服务器可能会多次收到确认通知
    echo "OK";
} else {
    //签名验证失败
}
?>