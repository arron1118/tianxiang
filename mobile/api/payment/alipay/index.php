<?php
header('Access-Control-Allow-Origin: *');
header('Content-type: text/plain');


class to_alipay{
// 对签名字符串转义
public function createLinkstring($para) {
    $arg  = "";
    while (list ($key, $val) = each ($para)) {
        $arg.=$key.'="'.$val.'"&';
    }
    //去掉最后一个&字符
    $arg = substr($arg,0,count($arg)-2);
    //如果存在转义字符，那么去掉转义
    if(get_magic_quotes_gpc()){$arg = stripslashes($arg);}
    return $arg;
}

// 签名生成订单信息
public  function rsaSign($data) {
    $priKey = "-----BEGIN RSA PRIVATE KEY-----
MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAMg/8eQ+PvveCdXU
qICy1JZCTiqxekk8cvs5hMr0D1A6vooISzIxSWSBie3n+8B4QTR9B6P1Udamhco4
KrzjWwdByVrvsMzPIn//7YAulPKRT8U6PdzoaqlilDLTwpTqj0SK6VM4ag4ecOVD
1cjZhG+MGyLT0PdU/XuTwOHBmtGnAgMBAAECgYEApoXefqJwt/x+C06rzVJpTIbY
/9HU0kAtkdLUJf3tGhevyZN6DMRFtwmuDKQEi5q1BpQOnX7szmZuDarFh4w9bgLM
UN0GXbP5fZPGK6vs/CFRYLExUyrPBlHzmIHjMLLSAC0tD2WDGJSnXfNCkRh0MTvK
62/Ux5+SBWhwAIOGOGkCQQD0ixhqnEES0U/rnTxfdJr/mCTYEuHkkRbm25lrA7K5
+oKq1SKeNHxVvuikUYoyqDUeH4J6B96PZnmwV8ddW2RTAkEA0aGek5uoErIMh1fQ
69aKv/vQHXbwfgzVZtBLjtep96L/6YPDT7/fZP1p7CRKZlTmRuZ/JrENvahiBsjE
688y3QJAUBJXfVkKibHVvG1wvkS9F+HmdoXAR4omeJMBKiQ82l6neG5vdmPzLlRj
cqJsYOfo9KMWowR+oG/Keq5TTkNFlwJAZXixFMw02A/dZqoIVBA+i12tyIVpNeqq
ZaEP4e3ctSTucS85nGHJFc5gtlB+vvf7m3g5NhZgC1z1TklioK5j9QJBAM+pkUAj
RU8Hhv5uOZyGIYBS4WoRSvRdhjeKFKUsIAElBShie9uPu/lxI9hIy1m3lWCIwmWk
dhCTySdfWhk+73o=
-----END RSA PRIVATE KEY-----";
    $res = openssl_get_privatekey($priKey);
    openssl_sign($data, $sign, $res);
    openssl_free_key($res);
    $sign = base64_encode($sign);
    $sign = urlencode($sign);
    return $sign;
}

public  function to_pay($order_list,$config,$notify_url){
   
     $appid=$config['alipay_account'];
    $key=$config['alipay_key'];
     $partner=$config['alipay_partner'];
// 获取支付金额
$amount='';
if($_SERVER['REQUEST_METHOD']=='POST'){
    $amount=$_POST['total'];
}else{
    $amount=$_GET['total'];
}
$amount=$order_list['order_list'][0]['order_amount'];

$total = floatval($amount);
if(!$total){
    $total = 1;
}

// 支付宝合作者身份ID，以2088开头的16位纯数字
$partner = $partner;
// 支付宝账号
$seller_id = $appid;
// 商品网址
$base_path = urlencode('http://shop.tianxiangmall.net/wap');
// 异步通知地址
$notify_url= urlencode($notify_url);
// 订单标题
$subject = $order_list['subject'];
// 订单详情
    
// 订单号，示例代码使用时间值作为唯一的订单ID号
$out_trade_no =$order_list['pay_sn'];

$parameter = array(
    'service'        => 'mobile.securitypay.pay',   // 必填，接口名称，固定值
    'partner'        => $partner,                   // 必填，合作商户号
    '_input_charset' => 'UTF-8',                    // 必填，参数编码字符集
    'out_trade_no'   => $out_trade_no,              // 必填，商户网站唯一订单号
    'subject'        => $subject,                   // 必填，商品名称
    'payment_type'   => '1',                        // 必填，支付类型
    'seller_id'      => $seller_id,                 // 必填，卖家支付宝账号
    'total_fee'      => $total,                     // 必填，总金额，取值范围为[0.01,100000000.00]
    'body'           => $order_list['subject'],                      // 必填，商品详情
    'it_b_pay'       => '1d',                       // 可选，未付款交易的超时时间
    'notify_url'     => $notify_url,                // 可选，服务器异步通知页面路径
    'call_back_url'     => 'www.baidu.com',
    'show_url'       => $base_path,                  // 可选，商品展示网站
        );
//生成需要签名的订单
$orderInfo = $this->createLinkstring($parameter);
//签名
$sign = $this->rsaSign($orderInfo);

//生成订单
echo $orderInfo.'&sign="'.$sign.'"&sign_type="RSA"';
}

}
?>
